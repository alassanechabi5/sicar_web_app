<?php
	
	if (!function_exists('convertirDate')) 
	{
	    function convertirDate($date)
	    {
	        // return \Carbon\Carbon::parse($date)->format(config('app.locale') != 'en' ? 'd/m/Y H:i:s' : 'm/d/Y H:i:s');

	        return \Carbon\Carbon::parse($date)->format('d/m/Y H:i:s');

	    }
	}

	if (!function_exists('convertirDateOnly')) 
	{
	    function convertirDateOnly($date)
	    {
	        return \Carbon\Carbon::parse($date)->format('d/m/Y');

	    }
	}


	if( !function_exists('generateSlug'))
	{
		function generateSlug($lenght = 23) {    
		    if (function_exists("random_bytes")) {
		        $bytes = random_bytes(ceil($lenght / 2));
		    } 
		    elseif (function_exists("openssl_random_pseudo_bytes")) {
		        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
		    } 
		    else {
		    	$bytes = uniqid() ;
		        // throw new Exception("no cryptographically secure random function available");
		    }
		    return substr(bin2hex($bytes), 0, $lenght);
		}
	}


	if( !function_exists('upgradeName'))
	{
		function upgradeName($nomComplet){
	        $pieces = explode(" ", $nomComplet);
	        $taille = count($pieces);
	        $dim = intval($taille-1);
	        $nom = "";

	        for($i=0; $i<$dim; $i++){
	            $nom = $nom." ".strtoupper($pieces[$i]);
	        }
	        $small = strtolower($pieces[$dim]);
	        $prenom = ucfirst($small);

	        $nom = trim($nom);

	        $nomFinal = $nom." ".$prenom;

	        return $nomFinal ;
	    }
	}

	if( !function_exists('csvToArray'))
	{
		function csvToArray($filename = '', $delimiter = ';')
		{
		    if (!file_exists($filename) || !is_readable($filename))
		        return false;

		    $header = null;
		    $data = array();
		    if (($handle = fopen($filename, 'r')) !== false)
		    {
		        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
		        {
		            if (!$header) {
                        $header = $row;
                    } else {
                        if (count($header) != count($row)) {
                            continue;
                        }
                        $data[] = array_combine($header, $row);
                    }
		        }
		        fclose($handle);
		    }

		    return $data;
		}
	}

	function send_notification_FCM($title, $message, $id, $type) 
	{

	    $accesstoken = env('FCM_KEY');

	    $URL = 'https://fcm.googleapis.com/fcm/send';


	        $post_data = '{
	            
	            "notification" : {
	                "body" : "' . $message . '",
	                "title" : "' . $title . '",
	                "type" : "' . $type . '",
	                "id" : "' . $id . '",
	                "message" : "' . $message . '",
	                "icon" : "new",
	                "sound" : "default"
	                },
	          }';
	       

	    $crl = curl_init();

	    $headr = array();
	    $headr[] = 'Content-type: application/json';
	    $headr[] = 'Authorization: ' . $accesstoken;
	    curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);

	    curl_setopt($crl, CURLOPT_URL, $URL);
	    curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);

	    curl_setopt($crl, CURLOPT_POST, true);
	    curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
	    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);

	    $rest = curl_exec($crl);

	    if ($rest === false) {
	        // throw new Exception('Curl error: ' . curl_error($crl));
	        //print_r('Curl error: ' . curl_error($crl));
	        $result_noti = 0;
	    } else {

	        $result_noti = 1;
	    }

	    //curl_close($crl);
	    //print_r($result_noti);die;
	    return $result_noti;
}
	