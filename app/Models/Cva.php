<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cva extends Model
{
    use HasFactory;

    protected $fillable = [
    	'intitule',
    	'description',
    	'path_img',
    	'slug'
    ];


}
