<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $fillable = [
    	'intitule',
    	'path_audio'
    ];


    public function modules()
    {
        return $this->hasMany(Module::class);
    }
}
