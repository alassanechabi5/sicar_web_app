<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relais extends Model
{
    use HasFactory;

    protected $fillable = [
    	'partenaire_id',
    	'full_name',
    	'ethnic_group',
    	'sexe',
    	'commune',
    	'village',
    	'phone',
    	'whatsapp',
    	'language',
    	'avatar',
        'age',
        'taille_menage',
        'actif_agricole',
        'respo',
        'type',
        'fiche_consentement',
    	'slug',
        'activite_principale',
        'activite_secondaire'
    ];


    public function partenaire()
    {
        return $this->belongsTo(Partenaire::class);
    }

    public function difficultes()
    {
        return $this->hasMany(Difficulte::class);
    }

    public function reponse_difficultes()
    {
        return $this->hasMany(ReponseDifficulte::class);
    }

    public function formations()
    {
        return $this->hasMany(Formation::class);
    }
}
