<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;

    protected $fillable = [
    	'relais_id',
    	'module_id',
        'language_id',
    	'note_evaluation',
    	'slug'
    ];


    public function relais()
    {
        return $this->belongsTo(Relais::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
