<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReponseDifficulte extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id',
    	'difficulte_id',
    	'relais_id',
        'cva_id',
    	'path_img',
    	'path_audio',
    	'description',
    	'slug',
        'read'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function difficulte()
    {
        return $this->belongsTo(Difficulte::class);
    }

    public function relais()
    {
        return $this->belongsTo(Relais::class);
    }

    public function cva()
    {
        return $this->belongsTo(Cva::class);
    }
}
