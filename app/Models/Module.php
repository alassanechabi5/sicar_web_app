<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    protected $fillable = [
    	'cva_id',
        'language_id',
    	'num_chapitre',
        'img_chapitre',
        'intitule',
    	'path_img',
    	'path_audio',
    	'path_video',
    	'description',        
    	'slug'
    ];


    public function cva()
    {
        return $this->belongsTo(Cva::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function formations()
    {
        return $this->hasMany(Formation::class);
    }
}
