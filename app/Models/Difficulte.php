<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Difficulte extends Model
{
    use HasFactory;

    protected $fillable = [
    	'relais_id',
        'cva_id',
    	'path_img',
    	'path_audio',
    	'slug',
        'local_id'
    ];


    public function relais()
    {
        return $this->belongsTo(Relais::class);
    }

    public function cva()
    {
        return $this->belongsTo(Cva::class);
    }
}
