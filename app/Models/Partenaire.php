<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partenaire extends Model
{
    use HasFactory;

    protected $fillable = [
    	'intitule',
    	'description',
    	'zone_intervention',
    	'path_img',
    	'path_audio',
    	'slug'
    ];
    
}
