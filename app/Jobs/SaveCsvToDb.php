<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Relais;

use DB;


class SaveCsvToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $csv_path;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($csv_path)
    {
        $this->csv_path = $csv_path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path_csv = $this->csv_path;

        $customerArr = array();
        $customerArr = csvToArray($path_csv);
        
        $taille = count($customerArr);
       
        for ($i = 0; $i <$taille ; $i ++)
        {
            $relais = new Relais;
            $relais->partenaire_id = 1;
            $relais->full_name = strtoupper($customerArr[$i]['nom'])." ".ucfirst($customerArr[$i]['prenom']);
            $relais->ethnic_group = $customerArr[$i]['ethnic_group'];
            $relais->sexe = $customerArr[$i]['sexe'];
            $relais->commune = $customerArr[$i]['commune'];
            $relais->village = $customerArr[$i]['village'];
            $relais->phone = $customerArr[$i]['phone'];
            $relais->whatsapp = $customerArr[$i]['whatsapp'];
            $relais->language = $customerArr[$i]['language'];
            $relais->avatar = null;
            $relais->age = $customerArr[$i]['age'];
            $relais->taille_menage = $customerArr[$i]['taille_menage'];
            $relais->respo = $customerArr[$i]['respo'];
            $relais->type = $customerArr[$i]['type'];
            $relais->actif_agricole = $customerArr[$i]['actif_agricole'];
            $relais->fiche_consentement = null;
            $relais->slug = generateSlug();
            $relais->save();
        }

        unlink($path_csv);
    }
}
