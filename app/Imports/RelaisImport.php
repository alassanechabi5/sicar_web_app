<?php

namespace App\Imports;

use App\Models\Relais;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;


class RelaisImport implements ToModel, WithHeadingRow, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Relais([
            'partenaire_id' => 1,
            'full_name' => (strtoupper($row['nom'])." ".ucfirst($row['prenom'])),
            'age' => $row['age'],            
            'sexe' => ($row['sexe']),
            'commune' => $row['commune'],
            'village' => ($row['village']),
            'phone' => $row['phone'],
            'whatsapp' => $row['whatsapp'],
            'taille_menage' => $row['taille_menage'],
            'actif_agricole' => $row['actif_agricole'],
            'respo' => $row['respo'],
            'type' => $row['type'],
            'language' => "",
            'avatar' => "",
            'ethnic_group' => "",            
            'fiche_consentement' => "",
            'slug' => generateSlug()
        ]);
    }


    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'ISO-8859-1'
        ];
    }
}

