<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Difficulte;
use App\Models\ReponseDifficulte;
use App\Models\Message;
use App\Models\Relais;

use DB;



class ReponseDifficulteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($relais_id)
    {
        /*$reponse_difficulte = ReponseDifficulte::where('relais_id', $relais_id)
                            ->get();
        $reponse = $reponse_difficulte->toArray();
        
        return response()->json($reponse, 200);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $folderAud = "/audios/reponsesDifficultes/";
        
        $reponse = new ReponseDifficulte;
        $reponse->user_id = Auth::user()->id;
        $reponse->difficulte_id = $request->difficulte_id;
        $reponse->relais_id = $request->relais_id;
        $reponse->cva_id = $request->cva_id;        
        

        if ($request->hasFile('path_audio')) 
        {
            $current_audio = $request->file('path_audio');
            $extensionAud = $current_audio->getClientOriginalExtension();
            $nomAudio = "audio-".date('d-m-Y').'-'.time().'.'.$extensionAud;
            $request->path_audio->move(public_path($folderAud), $nomAudio);
            $destinationsAud ='/public'.$folderAud.$nomAudio;
            $reponse->path_audio = $destinationsAud;
            $reponse->date_reponse = date('d-m-Y').' à '.time()
        }
        $reponse->slug = generateSlug();
        $reponse->save();

        /*$relais = Relais::where('id',$request->relais_id);
         
        $title = "Réponse à une difficulté ";
        $message = "Une réponse à été envoyé par rapport à une difficulté";
        $type = "Réponse";
        $partenaireId = $relais->partenaire_id;
        $users = User::where('partenaire_id',$partenaireId);
        foreach ($users as $user){
        send_notification_FCM($title, $message, $user->id, $type);
                 
        }*/

        return response()->json(['result' => "succes"], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $reponses = ReponseDifficulte::where('difficulte_id', $slug)
                            ->with('user','relais', 'cva')
                            ->get();

        $questions = Difficulte::where('id', $slug)
                    ->with('relais', 'cva')
                    ->get();
        return view('response.show', compact(['reponses','questions']));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
