<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

use App\Models\Partenaire;
use App\Models\Relais;

class PartenaireController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partenaires= Partenaire::all();

        $status=0;

        return view('partenaire.index', compact('partenaires','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $folderimg = "/images/partenaires/";
        $folderaud = "/audios/partenaires/";

        $partenaire = new Partenaire;
        
        
        $photos = $request->file('path_img');

       if($request->hasFile('path_img')){

                $extensionImg = $photos->getClientOriginalExtension();
                $nomImage = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
                $photos->move(public_path($folderimg), $nomImage);
                $destinationsImg = '/public'.$folderimg.$nomImage;
                $partenaire->path_img = $destinationsImg;
    
            }
         $audios = $request->file('path_audio');
    
               if($request->hasFile('path_audio')){

               $extensionAud = $audios->getClientOriginalExtension();
                    $nomAudio = "aud-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionAud;
                    $audios->move(public_path($folderaud), $nomAudio);
                    $destinationsAud = '/public'.$folderaud.$nomAudio;
                    $partenaire->path_audio = $destinationsAud;
                        }
        $partenaire->intitule = $request->intitule;
        $partenaire->description = $request->description;
        $partenaire->zone_intervention = $request->zone_intervention;
        $partenaire->slug = generateSlug();
        $partenaire->save();

        return redirect('/partenaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        $partenaire = Partenaire::where('slug',$slug)->first();

        $list_relais = Relais::with('partenaire')->where('partenaire_id', $partenaire->id)->get();

        return view('partenaire.show', compact('list_relais', 'partenaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $partenaires = Partenaire::where('slug',$slug)->first();

        return view('partenaire.edit',compact('partenaires'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {

        $partenaire = Partenaire::where('slug',$slug)->first();

        $folderImg = "/images/partenaires/";
        $folderAud = "/audios/partenaires/";

        $destinationsImg ="";
        $destinationsAud ="";

        $photos = $request->file('path_img');
        $audios = $request->file('path_audio');

         if($request->hasFile('path_img')){

                $extensionImg = $photos->getClientOriginalExtension();
                $nomImage = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
                $request->path_img->move(public_path($folderImg), $nomImage);

                $destinationsImg = '/public'.$folderImg.$nomImage;
        }
         

          if ($request->hasFile('path_audio')) 
        {
        
                $extensionAud = $audios->getClientOriginalExtension();
                $nomAudio = "audio-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionAud;
                $request->path_audio->move(public_path($folderAud), $nomAudio);

                $destinationsAud = '/public'.$folderAud.$nomAudio;
        }
        
                       
                $partenaire->intitule = $request->intitule ? $request->intitule : $partenaire->intitule;
                $partenaire->description = $request->description ? $request->description : $partenaire->description;
                $partenaire->zone_intervention = $request->zone_intervention ? $destinationsImg : $partenaire->path_img;
                $partenaire->path_img = $destinationsImg ? $destinationsImg : $partenaire->path_img;
                $partenaire->path_audio = $destinationsAud ? $destinationsAud : $partenaire->path_audio;


        $partenaire->save();

        $partenaires= Partenaire::all();

        $status = 0 ;

         

       return view('partenaire.index',compact('partenaires' , 'status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
