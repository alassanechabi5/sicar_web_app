<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Partenaire;
use App\Models\Role;



class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('role')->get();

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partenaires = Partenaire::all();
        $roles = Role::all();

        return view('user.create',compact('partenaires', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  $this->validate($request,[
        // 'avatar' => 'required|mimes:jpg',
        // ]);

        $destinationsImg="";
        $user = new User;

        $user->role_id = $request->role_id;
        $user->partenaire_id = $request->partenaire_id;
        $user->full_name = $request->full_name;
        $user->identifiant = $request->identifiant;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->country = $request->country;
        $user->town = $request->town;
        $user->phone1 = $request->phone1;
        $user->phone2 = $request->phone2;
        $user->slug = generateSlug();

        $folderProfil = "/images/profile/";
        $image = $request->file('avatar');

      if($request->hasFile('avatar')){

        $extensionImg = $image->getClientOriginalExtension();
        $nomAvatar = "avatar-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
        $image->move(public_path($folderProfil), $nomAvatar);
        $destinationsImg = '/public'.$destinationsImg.$folderProfil.$nomAvatar;

      }

        $user->avatar = $destinationsImg;
        $user->save();

        $users = User::with('role')->get();

        return view('user.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\View\View     */

       public function show($slug)
    {
        $user = User::where('slug',$slug)->first();
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $user = User::where('slug',$slug)->first();

        $partenaires = Partenaire::all();

        $roles = Role::all();

        return view('user.edit', compact('user', 'partenaires','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $one_user = User::where('slug',$slug)->first();

        $folderImg = "/images/profile/";
        $destinationsImg = "";

        $images = $request->file('avatar');

         if($request->hasFile('avatar')){
            if(is_file(public_path($one_user->avatar)))
            {
                unlink(public_path($one_user->avatar));
            }

            $extensionImg = $images->getClientOriginalExtension();
            $nomImg = "avatar-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
            $images->move(public_path($folderImg), $nomImg);
            $destinationsImg= '/public'.$folderImg.$nomImg;
            }

        $one_user->avatar = $destinationsImg ? $destinationsImg : $one_user->avatar;
        $one_user->role_id = $request->role_id ? $request->role_id : $one_user->role_id;
        $one_user->partenaire_id = $request->partenaire_id ? $request->partenaire_id : $one_user->partenaire_id;
        $one_user->full_name = $request->full_name ? $request->full_name : $one_user->full_name;
        $one_user->identifiant = $request->identifiant ? $request->identifiant : $one_user->identifiant;
        $one_user->email = $request->email ? $request->email : $one_user->email;
        $one_user->password = $request->password ? $request->password : $one_user->password;
        $one_user->country = $request->country ? $request->country : $one_user->country;
        $one_user->town = $request->town ? $request->town : $one_user->town;
        $one_user->phone1 = $request->phone1 ? $request->phone1 : $one_user->phone1;
        $one_user->phone2 = $request->phone2 ? $request->phone2 : $one_user->phone2;

        $one_user->save();

        $user = User::where('slug',$slug)->first();
        return view('user.show', compact('user'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
