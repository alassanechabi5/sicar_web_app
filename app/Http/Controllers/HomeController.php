<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Models\Relais;
use App\Models\Formation;
use App\Models\Message;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

    public function index()
    {
      $relais_commune = DB::table('relais')
                  ->select('commune', DB::raw('count(commune) as compte'))
                  ->groupBy('commune')
                  ->orderBy('commune' , 'asc')
                  ->orderBy('compte', 'asc')
                  ->get();

      $cours_suivis = Formation::with('module')->get();


      // $relais_formation = DB::table('formations') 
      //            ->select('module_id')
      //            ->select('relais_id', DB::raw('count(relais_id) as compterelais'))
      //            ->groupBy('relais_id','module_id')
      //            ->orderBy('compterelais', 'asc')
      //            ->orderBy('module_id', 'asc')
      //            ->get();
        return view('home.index', compact('relais_commune', 'cours_suivis'));
    }
}
