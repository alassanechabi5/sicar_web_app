<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\Models\Cva;

class CvaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //liste CVA
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $folder = "/images/cva/";

        $cva = new Cva;
        $cva->intitule = $request->intitule;
        $cva->description = $request->description;
        
        $photo = $request->file('path_img');
        $extension = $photo->getClientOriginalExtension();
        $nomFichier = "cva-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extension;
        $request->path_img->move(public_path($folder), $nomFichier);

        //$cva->path_img= "/public/".$folder.$nomFichier;
        $cva->path_img= $folder.$nomFichier;

        $cva->slug = generateSlug();
        $cva->save();

        return back()->with('status', "Cva ajoutée");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $cvas = Cva::where('slug',$slug)->first();

        return view('cva.edit',compact('cvas'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {

        $cva = Cva::where('slug',$slug)->first();

        $folderImg = "/images/cva/";

        $destinationsImg ="";

        $photos = $request->file('path_img');

         if($request->hasFile('path_img')){

            if (is_file(public_path($cva->path_img))) {
                unlink(public_path($cva->path_img));
            }
                $extensionImg = $photos->getClientOriginalExtension();
                $nomImage = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;

                $request->path_img->move(public_path($folderImg), $nomImage);

                $destinationsImg ='/public'.$folderImg.$nomImage;
        }

        $cva->intitule = $request->intitule ? $request->intitule: $cva->intitule;
        $cva->description = $request->description ? $request->description : $request->description ;
        $cva->path_img = $destinationsImg ? $destinationsImg : $cva->path_img;


        $cva->save();
        $cvas= Cva::all();

         $status = 0 ;    

        return view('module.index',compact('cvas' , 'status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
