<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\Models\Module;
use App\Models\Cva;
use App\Models\Language;

use DB;



class ModuleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cvas = Cva::all();        
        return view('module.index', compact('cvas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
   
    }


        /**
     * Show the form for creating a new resource.
     *@param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function createModule($slug)
    {
        $cva = Cva::where('slug',$slug)->first();
        $langues = Language::all();

        return view('module.create', compact('cva', 'langues'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
        'path_img' => 'required',
        'path_img.*' => 'mimes:png,jpeg',
        'path_audio' => 'required',
        'path_audio.*' => 'mimes:mp3,ogg,wav,mwa',
        'path_video' => 'required',
        'path_video.*' => 'mimes:mp4,avi,mkv,3gp,webm'
        ]);

        $folderImg = "/images/modules/";
        $folderAud = "/audios/modules/";
        $folderVid = "/videos/modules/";

        

        $cvaSlug = $request->slug;
        $cva = Cva::where('slug',$cvaSlug)->first();

        $module = new Module;
        $module->num_chapitre = $request->num_chapitre;
        $module->intitule = utf8_encode($request->intitule);
        $module->description = utf8_encode($request->description);
        $module->language_id = $request->language_id;
        $module->cva_id = $cva->id;
        $module->slug = generateSlug();


        $destinationsImg ="";
        $destinationsAud ="";
        $destinationsVid ="";

        $photos = $request->file('path_img');

        if ($request->hasFile('path_img')) 
        {
            foreach ($photos as $photo) 
            {
                $extensionImg = $photo->getClientOriginalExtension();
                $nomImage = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
                $photo->move(public_path($folderImg), $nomImage);

                $destinationsImg = $destinationsImg.'/public'.$folderImg.$nomImage.";";
            }

            $module->path_img = $destinationsImg;
        }


        $audios = $request->file('path_audio');

        if ($request->hasFile('path_audio')) 
        {
            foreach ($audios as $audio) 
            {
                $extensionAud = $audio->getClientOriginalExtension();
                $nomAudio = "audio-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionAud;
                $audio->move(public_path($folderAud), $nomAudio);

                $destinationsAud = $destinationsAud.'/public'.$folderAud.$nomAudio.";";
            }
            $module->path_audio = $destinationsAud;
        }

        
        $videos = $request->file('path_video');
            if ($request->hasFile('path_video')) 
        {
            foreach ($videos as $video) 
            {
                $extensionVid = $video->getClientOriginalExtension();
                $nomVideo = "video-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionVid;
                $video->move(public_path($folderVid), $nomVideo);

                $destinationsVid = $destinationsVid.'/public'.$folderVid.$nomVideo.";";
            }
            $module->path_video = $destinationsVid;
        }


        // $videos = $request->file(path_video);

        // foreach ($videos as $video) 
        // {
        //     $destinationsVid = $destinationsVid.$video.";";
        // }
        // $module->path_video = $destinationsVid;

        $module->save();

        return redirect('/modules/'.$cvaSlug);
    }

 
    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $slug_cva = $slug;
        $cva = Cva::where('slug',$slug)->first();

        $list_modules = Module::with('cva', 'language')->where('cva_id',$cva->id)->get();

        return view('module.show', compact('list_modules', 'slug_cva'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
         $module = Module:: where('slug',$slug)->first();
         $cva = CVA:: where('slug',$slug)->first();

         $langues = Language::all();

        return view('module.edit', compact('module','cva', 'langues'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $this->validate($request,[
        'path_img.*' => 'mimes:png,jpeg,webp,apng',
        'path_audio.*' => 'mimes:mp3,wav,wave,webm',
        'path_video.*' => 'mimes:mp4,avi,mkv,webm, 3gp'
        ]);

        $folderImg = "/images/modules/";
        $folderAud = "/audios/modules/";
        $folderVid = "/videos/modules/";

        $destinationsOldImg = "";
        $destinationsOldAud = "";
        $destinationsOldVid = "";
        $destinationsNewImg = "";
        $destinationsNewAud = "";
        $destinationsNewVid = "";

        $ce_module=Module::with('cva')->where('slug',$slug)->first();

        $liste_images= $ce_module->path_img;
        $tableau_images=explode(";", $liste_images);
        $taille_images = count($tableau_images);

        $liste_audios= $ce_module->path_audio;
        $tableau_audios=explode(";", $liste_audios);
        $taille_audios = count($tableau_audios);

        $liste_videos= $ce_module->path_video;
        $tableau_videos=explode(";", $liste_videos);
        $taille_videos = count($tableau_videos);

        // // Old elements input count
        // $imgCompte = count($_FILES['path_img']['name']);
        // $audCompte = count($_FILES['path_audio']['name']);
        // $vidCompte = count($_FILES['path_video']['name']);

        // // New elements
        $newImg = $request->file('path_images');
        $newAud = $request->file('path_audios');
        $newVid = $request->file('path_videos');

        
        //Stock the old elements files
        if ($request->hasFile('path_img')) { 
            for ($i=0; $i < $taille_images-1 ; $i++) { 

                $photo = $_FILES['path_img']['name'][$i]; //Get value of the input file submitted
                // dd($photo);

                if (empty($photo)) {
                    $destinationsOldImg = $destinationsOldImg.'/public'.$tableau_images[$i].";";
                }else{
                    $temp_file = $request->file('path_img')[$i];
                    $info_photo = pathinfo($photo);
                    $extensionImg = $temp_file->getClientOriginalExtension();
                    $nomImg = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
                    $temp_file->move(public_path($folderImg), $nomImg);
                    unlink(public_path($tableau_images[$i]));
                    $destinationsOldImg = $destinationsOldImg.'/public'.$folderImg.$nomImg.";";
                }
            }
        }else{$destinationsOldImg = $liste_images;}
        

        if ($request->hasFile('path_audio')) { 
            for ($j=0; $j < $taille_audios-1 ; $j++) { 

                $audio = $_FILES['path_audio']['name'][$j]; //Get value of the input file submitted

                if (empty($audio)) {
                    $destinationsOldAud = $destinationsOldAud.'/public'.$tableau_audios[$j].";";
                }else{
                    $temp_file = $request->file('path_audio')[$j];
                    $info_audio = pathinfo($audio);
                    $extensionAud = $temp_file->getClientOriginalExtension();
                    $nomAud = "audio-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionAud;
                    $temp_file->move(public_path($folderAud), $nomAud);
                    unlink(public_path($tableau_audios[$j]));
                    $destinationsOldAud = $destinationsOldAud.'/public'.$folderAud.$nomAud.";";
                }
            }
        }else{$destinationsOldAud = $liste_audios;}
        

        if ($request->hasFile('path_img')) { 
            for ($k=0; $k < $taille_videos-1 ; $k++) { 

                $video = $_FILES['path_video']['name'][$k]; //Get value of the input file submitted

                if (empty($video)) {
                    $destinationsOldVid = $destinationsOldVid.'/public'.$tableau_videos[$k].";";
                }else{
                    $temp_file = $request->file('path_video')[$k];
                    $info_video = pathinfo($video);
                    $extensionVid = $temp_file->getClientOriginalExtension();
                    $nomVid = "video-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionVid;
                    $temp_file->move(public_path($folderVid), $nomVid);
                    unlink(public_path($tableau_videos[$k]));
                    $destinationsOldVid = $destinationsOldVid.'/public'.$folderVid.$nomVid.";";
                }
            }
        }else{$destinationsOldVid = $liste_videos;}
        

        //     //Stock the new elements files

        if ($request->hasFile('path_images')) 
        {   
            foreach($newImg as $one_img)
            {
                $extensionImg = $one_img->getClientOriginalExtension();
                $nomImg = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
                $one_img->move(public_path($folderImg), $nomImg);
                $destinationsNewImg = $destinationsNewImg.'/public'.$folderImg.$nomImg.";";
            }
        }

        if ($request->hasFile('path_audios')) 
        {   
            foreach($newAud as $one_aud)
            {
                $extensionAud = $one_aud->getClientOriginalExtension();
                $nomAud = "audio-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionAud;
                $one_aud->move(public_path($folderAud), $nomAud);
                $destinationsNewAud = $destinationsNewAud.'/public'.$folderAud.$nomAud.";";
            }
        }

        if ($request->hasFile('path_videos')) 
        {   
            foreach($newVid as $one_vid)
            { 
                $extensionVid = $one_vid->getClientOriginalExtension();
                $nomVid = "video-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionVid;
                $one_vid->move(public_path($folderVid), $nomVid);
                $destinationsNewVid = $destinationsNewVid.'/public'.$folderVid.$nomVid.";";
            }
        }

        $destinationsImg = $destinationsOldImg.$destinationsNewImg;
        $destinationsAud = $destinationsOldAud.$destinationsNewAud;
        $destinationsVid = $destinationsOldVid.$destinationsNewVid;

       
        $ce_module->cva_id = $ce_module->cva_id;
        $ce_module->language_id = $request->language_id ? $request->language_id : $ce_module->language_id;
        $ce_module->num_chapitre = $request->num_chapitre ? $request->num_chapitre : $ce_module->num_chapitre;
        $ce_module->intitule = $request->intitule ? $request->intitule : $ce_module->intitule;
        $ce_module->description = $request->description ? $request->description : $ce_module->description;
        $ce_module->path_video = $destinationsVid;
        $ce_module->path_img = $destinationsImg;
        $ce_module->path_audio = $destinationsAud;

        $ce_module->save();
        $cvaSlug = $ce_module->cva->slug;

        return redirect('/modules/'.$cvaSlug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ce_module = Module::find($id);
        $cvaId = $ce_module->cva_id;
        $cva = Cva::where('id',$cvaId)->get();
        $cvaslug = $cva->slug; 
        $ce_module->formations()->delete();
        
        DB::table('modules')->where('id',$id)->delete();

        return redirect('/modules/'.$cvaslug);
    }
}
