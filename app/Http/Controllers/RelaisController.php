<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


use App\Jobs\SaveCsvToDb;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\RelaisImport;

use App\Models\Relais;
use App\Models\Difficulte;
use App\Models\Partenaire;
use App\Models\Language;
use DB;


class RelaisController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relais= Relais::with('partenaire')->get();
        $langues = Language::all();
        $communes = DB::table('relais')
        ->select('commune')
        ->groupBy('commune')
        ->orderBy('commune' , 'asc')
        ->get();

        return  view('relais.index', compact('relais','langues','communes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $current_relais = Relais::where('slug', $slug)->first();

        $difficultes = Difficulte::with('relais')
                            ->where('relais_id',$current_relais->id)
                            ->get();

        return view('relais.show', compact('difficultes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function advancedFilter()
    {
        // ss
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $relais = Relais::where('slug', $slug)->first();;
        $partenaires = Partenaire::all();
        $langages = Language::all();

        return view('relais.edit', compact('relais','partenaires','langages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $one_relais = Relais::where('slug',$slug)->first();

        $folderImg = "/images/relais/";

        $destinationsImg ="";

        $photos = $request->file('avatar');

         if($request->hasFile('avatar')){
            if(is_file(public_path($one_relais->avatar)))
            {
                unlink(public_path($one_relais->avatar));
            }
                $extensionImg = $photos->getClientOriginalExtension();
                $nomImage = "img-".Str::random(10)."-".date('d-m-Y').'-'.time().'.'.$extensionImg;
                $request->avatar->move(public_path($folderImg), $nomImage);

                $destinationsImg ='/public'.$folderImg.$nomImage;
            }
                      
            $one_relais->partenaire_id = $request->partenaire_id ? $request->partenaire_id : $one_relais->partenaire_id;
            $one_relais->full_name = $request->full_name ? $request->full_name : $one_relais->full_name;
            $one_relais->ethnic_group = $request->ethnic_group ? $request->ethnic_group : $one_relais->ethnic_group;
            $one_relais->sexe = $request->sexe ? $request->sexe : $one_relais->sexe;
            $one_relais->commune = $request->commune ? $request->commune : $one_relais->commune;
            $one_relais->village = $request->village ? $request->village : $one_relais->village;
            $one_relais->phone = $request->phone ? $request->phone : $one_relais->phone;
            $one_relais->whatsapp = $request->whatsapp ? $request->whatsapp : $one_relais->whatsapp;
            $one_relais->language = $request->language ? $request->language : $one_relais->language;
            $one_relais->age = $request->age ? $request->age : $one_relais->age;
            $one_relais->taille_menage = $request->taille_menage ? $request->taille_menage : $one_relais->taille_menage;
            $one_relais->actif_agricole = $request->actif_agricole ? $request->actif_agricole : $one_relais->actif_agricole;
            $one_relais->respo = $request->respo ? $request->respo : $one_relais->respo;
            $one_relais->avatar = $destinationsImg ? $destinationsImg : $one_relais->avatar;

        $one_relais->save();

        $relais= Relais::with('partenaire')->get(); 
        $langues = Language::all();
        $communes = DB::table('relais')
        ->select('commune')
        ->groupBy('commune')
        ->orderBy('commune' , 'asc')
        ->get();        

    return  view('relais.index', compact('relais','langues','communes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ce_relais = Relais::find($id);

    //Suppression des relations en lien avec ce relais
        $ce_relais->difficultes()->delete();
        $ce_relais->formations()->delete();
        $ce_relais->reponse_difficultes()->delete();

        DB::table('relais')->where('id',$id)->delete();

        $relais= Relais::with('partenaire')->get();

        return view('relais.index',compact('relais'));
    }


    public function storeFileDb(Request $request)
    {
        Excel::import(new RelaisImport, $request->file('csv_file')->store('temp'));

        return redirect('/relais');
    }

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

public function rechercher(Request $request){

    $commune=$request->commune; 
    $sexe=$request->sexe;
    $langue = $request-> langue;
    $liste_relais= Relais::with('partenaire');

    if($commune !== 'all'){
        $liste_relais->where('commune',$commune);
    }

    // if($langue !==all){

    //     $liste_relais->where('langue',$langue)->get;
    // }

    if($sexe !== 'all'){
    $liste_relais->where('sexe',$sexe);

    }
    
    $relais = $liste_relais->get();


    // $langues = Language::all();
    $communes = DB::table('relais')
        ->select('commune')
        ->groupBy('commune')
        ->orderBy('commune' , 'asc')
        ->get();

    return view('relais.index',compact('relais','communes'));
    }

}
