<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Language;
use DB;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $langues = Language::all();
        return view('language.index', compact('langues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $this->validate($request,[
        'path_audio' => 'required|mimes:mp3,ogg,wav,mwa',
        ]);

        $folder = "/audios/";

        $one_language = new Language;

        $audios = $request->file('path_audio');

        if ($request->hasFile('path_audio')) {
            $extensionAud = $audios->getClientOriginalExtension();
            $nomAud = $request->intitule.'.'.$extensionAud;
            $audios->move(public_path($folder), $nomAud);
            $destinationAud= '/public'.$folder.$nomAud;
            $one_language->path_audio = $destinationAud;
        }

        $one_language->intitule = $request->intitule;

        $one_language->save();

        $langues = Language::all();




        return view('language.index',compact('langues') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $langues = Language::where('slug',$slug)->first();

        return view('language.edit',compact('langues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
          $this->validate($request,[
        'path_audio' => 'mimes:mp3,ogg,wav,mwa',
        ]);

         $one_language = Language::where('slug',$slug)->first();

        $folderAud = "/audios/";
        $destinationAud ="";

        $audios = $request->file('path_audio');

         if($request->hasFile('path_audio')){
            if(is_file(public_path($one_language->path_audio)))
            {
                unlink(public_path($one_language->path_audio));
            }

            $extensionAud = $audios->getClientOriginalExtension();
            $nomAud = $request->intitule.'.'.$extensionAud;
            $audios->move(public_path($folderAud), $nomAud);
            $destinationAud= '/public'.$folderAud.$nomAud;
            }

        $one_language->path_audio = $destinationAud ? $destinationAud : $one_language->path_audio;
        $one_language->intitule = $request->intitule ? $request->intitule : $one_language->intitule;

        $one_language->save();

        $langues = Language::all();

        return view('language.index',compact('langues') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $one_language = Language::find($id);

        $one_language->modules()->delete();

            if(is_file(public_path($one_language->path_audio)))
            {
                unlink(public_path($one_language->path_audio));
            }

        DB::table('languages')->where('id',$id)->delete();

        $langues = Language::all();

        return view('language.index',compact('langues') );
    }
}
