<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use App\Models\Partenaire;

use DB;

class PartenaireApiController extends Controller
{
    public function index()
    {
    	$partners = Partenaire::all();
    	$partenaires = $partners->toArray();
      	return $partenaires;
    }
}
