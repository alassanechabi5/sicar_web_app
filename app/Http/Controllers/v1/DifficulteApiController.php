<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use App\Models\Relais;
use App\Models\Difficulte;

use DB;

class DifficulteApiController extends Controller
{
    public function index($difficultesId)
    {
        $rep = Difficulte::where('id', $difficultesId)->get();
        $reponses = $rep->toArray();
        return $reponses;
    }
    
    public function store(Request $request)
    {
    	$folder = "uploads_api/";
        
        $difficulte = new Difficulte;
        $difficulte->relais_id = $request->relais_id;
        $difficulte->cva_id = $request->cva_id;
        $difficulte->local_id = $request->local_id;

        if($request->has('image'))
        {
        
        $img = "img_".time().".jpeg"; 
        $cible1 = public_path($folder.$img) ;
        $tmp_name= $request->file('image')->getPathName(); 
        move_uploaded_file($tmp_name, $cible1);
        $difficulte->path_img = $folder.$img;
        }
        else {
            $difficulte->path_img = "";
        }

        $audio = "audio_".time().".wav";
        $cible2 = public_path($folder.$audio) ;
        $tmp_name= $request->file('audio')->getPathName();
        move_uploaded_file($tmp_name, $cible2);
        $difficulte->path_audio = $folder.$audio;
        $difficulte->slug = generateSlug();
        $difficulte->save();
  

    	return response()->json(['result' => true], 200);
    }
}
