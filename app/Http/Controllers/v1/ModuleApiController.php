<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use App\Models\Relais;
use App\Models\Difficulte;
use App\Models\Cva;
use App\Models\Module;
use App\Models\Language;
use App\Models\Formation;

use DB;

class ModuleApiController extends Controller
{
    public function indexLangue()
    {
    	$list_langues = Language::all();
    	$langues = $list_langues->toArray();
      	
        return response()->json($langues, 200);
    }


    public function indexCva()
    {
    	$list_cva = Cva::all();
    	$cvas = $list_cva->toArray();
      	
        return response()->json($cvas, 200);
    }


    public function listModule(Request $request)
    {
        /*$cva_id = $request->cva_id;*/
        $language_id = $request->language_id;
    	/*$cva_id = 4;
    	$language_id = 1;*/

    	/*$modules_cva = Module::where('cva_id', $cva_id)
    						->where('language_id', $language_id)
    						->get()*/

        $modules_cva = Module::where('language_id', $language_id)
                            ->get();

    	$modules = $modules_cva->toArray();
      	
        return response()->json($modules, 200);
    }

    public function storeEvaluation(Request $request)
    {
        $evaluation = new Formation;
        $evaluation->relais_id = $request->relais_id;
        $evaluation->module_id = $request->module_id;
        $evaluation->language_id = $request->language_id;
        $evaluation->note_evaluation = $request->note_evaluation;
        $evaluation->slug = generateSlug();
        $evaluation->save();

        return response()->json(['result' => true], 201);
    }
}
