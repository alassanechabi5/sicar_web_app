<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use App\Models\Relais;

use DB;

class RelaisApiController extends Controller
{
    public function index()
    {
        $relais = Relais::all();
        $relais = $relais->toArray();
        
        return response()->json($relais, 200);
    }
    
    
    public function store(Request $request)
    {
    	$relais = new Relais;
    	$relais->partenaire_id = $request->partenaire_id;
    	$relais->full_name = strtoupper($request->last_name)." ".ucfirst($request->first_name);
    	$relais->ethnic_group = $request->ethnic_group;
    	$relais->sexe = $request->sexe;
    	$relais->commune = $request->commune;
    	$relais->village = $request->village;
    	$relais->phone = $request->phone;
    	$relais->whatsapp = $request->whatsapp;
    	$relais->language = $request->language;
    	$relais->avatar = "";
        $relais->respo = strtoupper($request->last_name)." ".ucfirst($request->first_name);
        $relais->type = "Relais";
        $relais->activite_principale = $request->activite_principale;
        $relais->activite_secondaire = $request->activite_secondaire;
    	$relais->slug = generateSlug();
    	$relais->save();

    	return response()->json(['relais_id' => $relais->id], 201);
    }


    public function update(Request $request)
    {
        $id = $request->id;

        $relais = Relais::findOrFail($id);
        if($relais)
        {
            $relais->partenaire_id = $request->partenaire_id;
            $relais->full_name = strtoupper($request->last_name)." ".ucfirst($request->first_name);
            $relais->ethnic_group = $request->ethnic_group;
            $relais->sexe = $request->sexe;
            $relais->commune = $request->commune;
            $relais->village = $request->village;
            $relais->phone = $request->phone;
            $relais->whatsapp = $request->whatsapp;
            $relais->language = $request->language;
            $relais->save();
            return response()->json(['relais_id' => $relais->id], 200);
        }
        else {
            return response()->json(['result' => false], 404);
        }
    }


    public function checkPhone(Request $request)
    {
        $phone = $request->phone;

        $is_present = Relais::where('phone', $phone)->first();

        if($is_present)
        {
            return response()->json($is_present, 200);
        }
        else{
            return response()->json(['result' => false], 404);
        }
    }
}
