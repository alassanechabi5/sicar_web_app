<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use App\Models\ReponseDifficulte;

use DB;

class ReponseDifficulteApiController extends Controller
{

    public function isInternet()
    {
    	$element = ["response : yes"];
      	
        return response()->json($reponses, 200);
    } 

    public function index($relais_id)
    {
        $rep = ReponseDifficulte::where('relais_id', $relais_id)->get();
        $reponses = $rep->toArray();
        return $reponses;
    }

    public function show($relais_id)
    {
        $rep = ReponseDifficulte::where('relais_id', $relais_id)
        ->where('read','No')
        ->get();
        $reponses = $rep->toArray();
        return response()->json($reponses, 200);
    }

    public function update($relais_id)
    {
        $rep = ReponseDifficulte::where('relais_id', $relais_id)
        ->get();
        for ($i=0; $i <count($rep) ; $i++) { 
            $response = ReponseDifficulte::find($rep[$i]->id);
            $response ->read = "Yes";
            $response ->save();
        }
        
        $reponses = $rep->toArray();
        return response()->json($reponses, 200);
    }
}
