<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RelaisController;
use App\Http\Controllers\PartenaireController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\DifficulteController;
use App\Http\Controllers\FormationController;
use App\Http\Controllers\CvaController;
use App\Http\Controllers\ReponseDifficulteController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\SampleController::class, 'racine']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('/users', UserController::class);
Route::get('/users/{user:slug}/edit', [App\Http\Controllers\UserController::class,'edit']);
Route::put('/users/{user:slug}', [App\Http\Controllers\UserController::class,'update']);



Route::resource('/relais', RelaisController::class);
Route::get('/relais/{relais:slug}/edit', [App\Http\Controllers\RelaisController::class,'edit']);
Route::put('/relais/{relais:slug}', [App\Http\Controllers\RelaisController::class,'update']);
Route::post('/relais-file', [App\Http\Controllers\RelaisController::class, 'storeFileDb']);

Route::resource('/partenaires', PartenaireController::class);

Route::get('/partenaires/{partenaire:slug}/edit', [App\Http\Controllers\PartenaireController::class,'edit']);
Route::put('/partenaires/{partenaire:slug}', [App\Http\Controllers\PartenaireController::class,'update']);

Route::resource('/modules', ModuleController::class);
Route::get('/modules/{module:slug}/edit', [App\Http\Controllers\ModuleController::class,'edit']);
Route::put('/modules/{module:slug}', [App\Http\Controllers\ModuleController::class,'update']);

Route::resource('/difficultes', DifficulteController::class);
Route::resource('/formations', FormationController::class);

Route::resource('/cvas', CvaController::class);
Route::get('/cvas/{cva:slug}/edit', [App\Http\Controllers\CvaController::class,'edit']);
Route::put('/cvas/{cva:slug}', [App\Http\Controllers\CvaController::class,'update']);


Route::resource('/reponseDifficultes', ReponseDifficulteController::class);
Route::get('/reponseDifficultes/{reponseDifficulte:slug}', [App\Http\Controllers\ReponseDifficulteController::class,'show']);

Route::resource('/languages', LanguageController::class);
Route::get('/languages/{language:slug}/edit', [App\Http\Controllers\LanguageController::class,'edit']);
Route::put('/languages/{language:slug}', [App\Http\Controllers\LanguageController::class,'update']);


Route::get('/modules-creation/{cva:slug}', [App\Http\Controllers\ModuleController::class, 'createModule']);

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::post('/relaisRechercher', [App\Http\Controllers\RelaisController::class, 'rechercher']);

// Route::get('/pushNotificationController', [App\Http\Controllers\pushNotificationController::class, 'sendPushNotification']);