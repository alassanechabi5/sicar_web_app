<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\v1;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/relais', [App\Http\Controllers\v1\RelaisApiController::class, 'index']);
Route::post('/relais', [App\Http\Controllers\v1\RelaisApiController::class, 'store']);
Route::put('/relais', [App\Http\Controllers\v1\RelaisApiController::class, 'update']);

Route::post('/difficultes', [App\Http\Controllers\v1\DifficulteApiController::class, 'store']);
Route::get('/difficultes/{difficultesId}', [App\Http\Controllers\v1\DifficulteApiController::class, 'index']);

Route::get('/reponses/{relais_id}', [App\Http\Controllers\v1\ReponseDifficulteApiController::class, 'index']);
Route::get('/isInternet', [App\Http\Controllers\v1\ReponseDifficulteApiController::class, 'isInternet']);

Route::get('/check_read/{relais_id}', [App\Http\Controllers\v1\ReponseDifficulteApiController::class, 'update']);

Route::get('/reponseslist/{relais_id}', [App\Http\Controllers\v1\ReponseDifficulteApiController::class, 'show']);

Route::get('/partenaires', [App\Http\Controllers\v1\PartenaireApiController::class, 'index']);

Route::get('/languages', [App\Http\Controllers\v1\ModuleApiController::class, 'indexLangue']);

Route::get('/cvas', [App\Http\Controllers\v1\ModuleApiController::class, 'indexCva']);

Route::post('/modules-api', [App\Http\Controllers\v1\ModuleApiController::class, 'listModule']);

Route::post('/evaluations', [App\Http\Controllers\v1\ModuleApiController::class, 'storeEvaluation']);

Route::post('/phone', [App\Http\Controllers\v1\RelaisApiController::class, 'checkPhone']);
