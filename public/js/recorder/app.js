function timeToString(time) {
  let diffInHrs = time / 3600000;
  let hh = Math.floor(diffInHrs);

  let diffInMin = (diffInHrs - hh) * 60;
  let mm = Math.floor(diffInMin);

  let diffInSec = (diffInMin - mm) * 60;
  let ss = Math.floor(diffInSec);

  let diffInMs = (diffInSec - ss) * 100;
  let ms = Math.floor(diffInMs);

  let formattedMM = mm.toString().padStart(2, "0");
  let formattedSS = ss.toString().padStart(2, "0");
  // let formattedMS = ms.toString().padStart(2, "0");

  // return `${formattedMM}:${formattedSS}:${formattedMS}`;
  return `${formattedMM}:${formattedSS}`;
}

// Declare variables to use in our functions below

let startTime;
let elapsedTime = 0;
let timerInterval;
var file_audio;
var audio_fragments = [];



function print(txt) {
  document.getElementById("display").innerHTML = txt;
}

function pause() {
  clearInterval(timerInterval);
  mediaRecorder.pause();
  showButton("Jouer");
}
    


function showButton(buttonKey) {
  const buttonToShow = buttonKey === "Jouer" ? playButton : pauseButton;
  const buttonToHide = buttonKey === "Jouer" ? pauseButton : playButton;
  buttonToShow.style.display = "block";
  buttonToHide.style.display = "none";
}

pauseButton.addEventListener("click", pause);

playButton.onclick = () =>{
    
            navigator.mediaDevices.getUserMedia({audio:true}) //Demander l'accès à l'utilisateur d'utiliser le microphone pour enregistrer
                .then(stream => {
                               
                    mediaRecorder = new MediaRecorder(stream) 

                    mediaRecorder.mimeType = 'audio/mpeg'

                    mediaRecorder.start() 

                    startTime = Date.now() - elapsedTime;
                    timerInterval = setInterval(function printTime() {
                        elapsedTime = Date.now() - startTime;
                        print(timeToString(elapsedTime));
                         }, 10);
                    
                    showButton("Pause");
                    

                    mediaRecorder.addEventListener("dataavailable", e =>{
                        audio_fragments.push(e.data, {'type':'audio/wav'})
                    })

                    mediaRecorder.addEventListener("stop", e =>{

                        var blob = new Blob(audio_fragments, {type:'audio/mpeg'})
                        var audio_url = URL.createObjectURL(blob)
                  
                        var audio_element = document.createElement('audio')                   
                        audio_element.controls = true
                        audio_element.src = audio_url;

                        file_audio = blob;

                        disp_audio.innerHTML= "";
                        disp_audio.appendChild(audio_element)
                    })
            })
        }


stopButton.onclick = () =>{
    mediaRecorder.stop()

    clearInterval(timerInterval);
    print("00:00");
    elapsedTime = 0;
    showButton("Jouer");
}
