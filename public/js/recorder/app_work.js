

// Convert time to a format of hours, minutes, seconds, and milliseconds

function timeToString(time) {
  let diffInHrs = time / 3600000;
  let hh = Math.floor(diffInHrs);

  let diffInMin = (diffInHrs - hh) * 60;
  let mm = Math.floor(diffInMin);

  let diffInSec = (diffInMin - mm) * 60;
  let ss = Math.floor(diffInSec);

  let diffInMs = (diffInSec - ss) * 100;
  let ms = Math.floor(diffInMs);

  let formattedMM = mm.toString().padStart(2, "0");
  let formattedSS = ss.toString().padStart(2, "0");
  let formattedMS = ms.toString().padStart(2, "0");

  return `${formattedMM}:${formattedSS}:${formattedMS}`;
}

// Declare variables to use in our functions below

let startTime;
let elapsedTime = 0;
let timerInterval;

// Create function to modify innerHTML

function print(txt) {
  document.getElementById("display").innerHTML = txt;
}

// Create "start", "pause" and "reset" functions

// function start() {
//   startTime = Date.now() - elapsedTime;
//   timerInterval = setInterval(function printTime() {
//     elapsedTime = Date.now() - startTime;
//     print(timeToString(elapsedTime));
//   }, 10);
//   showButton("PAUSE");
// }

function pause() {
  clearInterval(timerInterval);
  mediaRecorder.pause()
  showButton("PLAY");
}

// function conversion(fblob) {

//     var ffmpeg = require('ffmpeg');


//         var process = new ffmpeg(fblob);
//         process.then( function (audio)){

//            reponse = audio.fnExtractSoundToMP3('/audios/reponses/');
//     }
//     return reponse;
// }

// function reset() {
//   clearInterval(timerInterval);
//   print("00:00:00");
//   elapsedTime = 0;
//   showButton("PLAY");
// }

// Create function to display buttons

function showButton(buttonKey) {
  const buttonToShow = buttonKey === "PLAY" ? playButton : pauseButton;
  const buttonToHide = buttonKey === "PLAY" ? pauseButton : playButton;
  buttonToShow.style.display = "block";
  buttonToHide.style.display = "none";
}

pauseButton.addEventListener("click", pause);

playButton.onclick = () =>{
            navigator.mediaDevices.getUserMedia({audio:true}) //Demander l'accès à l'utilisateur d'utiliser le microphone pour enregistrer
                .then(stream => {
                               
                mediaRecorder = new MediaRecorder(stream) //
                
                mediaRecorder.start() //Lancer l'enregistrement
                startTime = Date.now() - elapsedTime;
                timerInterval = setInterval(function printTime() {
                    elapsedTime = Date.now() - startTime;
                    print(timeToString(elapsedTime));
                     }, 10);
                  showButton("PAUSE");
                chuck = []

                mediaRecorder.addEventListener("dataavailable", e =>{
                    chuck.push(e.data, {'type':'audio/wav'}) //Ajoute les enregistrements l'un à la suite de l'autre
                })

            mediaRecorder.addEventListener("stop", e =>{
                    blob = new Blob(chuck, { 'type' : 'audio/wav; codecs=0' })
                    audio_url = window.URL.createObjectURL(blob)
                    au = document.createElement('audio')
                    //add controls to the <audio> element 
                    au.controls = true
                    au.src = audio_url;

                    //add the new audio and a elements to the li element 
                    ok.appendChild(au)
                    // document.getElementById("path_audio").setAttribute("value",blob)

                    //add the li element to the ordered list
                    // var filename = new Date().toISOString()


                })
            })
        }

    stopButton.onclick = () =>{
        mediaRecorder.stop()

        clearInterval(timerInterval);
        print("00:00:00");
        elapsedTime = 0;
        showButton("PLAY");
        }


      envoyer.onclick = () =>{
        var filename = new Date().toISOString();

                    var xhr = new XMLHttpRequest();
                        xhr.onload = function(e) {
                            if (this.readyState === 4) {
                                console.log("Server returned: ", e.target.responseText);
                            }
                        };
                        var fd = new FormData();
                        fd.append("path_audio", blob, filename);
                        xhr.open("POST", "{{'/reponseDifficultes'}}", true);
                        xhr.send(fd);
      }