<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActiviteSecondaireToRelaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relais', function (Blueprint $table) {
        $table->string('activite_secondaire')->nullable()->after('activite_principale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relais', function (Blueprint $table) {
                      $table->dropColumn('activite_secondaire');

        });
    }
}
