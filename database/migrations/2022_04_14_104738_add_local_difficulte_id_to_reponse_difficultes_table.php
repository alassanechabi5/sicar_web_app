<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocalDifficulteIdToReponseDifficultesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reponse_difficultes', function (Blueprint $table) {
        $table->string('local_difficulte_id')->nullable()->after('difficulte_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reponse_difficultes', function (Blueprint $table) {
          $table->dropColumn('local_difficulte_id');

        });
    }
}
