<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relais', function (Blueprint $table) {
            $table->id();
            $table->foreignId('partenaire_id')->constrained();
            $table->string('full_name')->nullable();
            $table->string('ethnic_group')->nullable();
            $table->string('sexe')->nullable();
            $table->string('commune')->nullable();
            $table->string('village')->nullable();
            $table->string('phone');
            $table->string('whatsapp')->nullable();
            $table->string('language')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('age')->nullable();
            $table->integer('taille_menage')->nullable();
            $table->integer('actif_agricole')->nullable();
            $table->string('respo');
            $table->string('type')->default('Relais');
            $table->string('fiche_consentement')->nullable();
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relais');
    }
}
