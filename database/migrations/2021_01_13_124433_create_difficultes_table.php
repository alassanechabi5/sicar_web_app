<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDifficultesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('difficultes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('relais_id')->constrained();
            $table->foreignId('cva_id')->constrained();
            $table->string('path_img')->nullable();
            $table->string('path_audio'); 
            $table->string('slug')->unique();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('difficultes');
    }
}
