<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RelaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::count()==0)
        {
        	DB::table('users')->insert([
        		'role_id' => 1,
        		'partenaire_id' => 1,
        		'full_name' => 'ADMIN User',
        		'identifiant' => 'admin',
        		'email' => 'admin@gmail.com',
        		'password'=> Hash::make('admin_user'),
        		'country' => 'Bénin',
        		'town' => 'Natitingou',
        		'phone1'=> '95010203',
        		'phone2'=> '95010203',
        		'avatar'=> '/images/avatar-1.png',
        		'slug' => generateSlug(),
        		'remember_token' => Str::random(10),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d') 
        	]);
        }    }
}
