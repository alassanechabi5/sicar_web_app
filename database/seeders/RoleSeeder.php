<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Role::count()==0)
        {
        	DB::table('roles')->insert([
        		'intitule' => 'Administrateur',
        		'level' => 1,
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d') 
        	]);

        	DB::table('roles')->insert([
        		'intitule' => 'Responsable Partenaire',
        		'level' => 2,
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d') 
        	]);

        	DB::table('roles')->insert([
        		'intitule' => 'Lecture et ecriture',
        		'level' => 3,
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d') 
        	]);

        	DB::table('roles')->insert([
        		'intitule' => 'Lecture',
        		'level' => 4,
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d') 
        	]);
        }
    }
}
