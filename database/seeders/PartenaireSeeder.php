<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Partenaire;

class PartenaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Partenaire::count()==0)
        {
        	DB::table('partenaires')->insert([
        		'intitule' => 'RBT-WAP/GIC-WAP',
        		'description'=> "Programme de l'environnement",
        		'zone_intervention' => 'Communes riveraines du complexe W-Arly-Pendjari',
        		'path_img'=> '/images/logo-rbt-wap.png',
        		'path_audio' => '/audios/audio.mp3',
        		'slug'=> generateSlug(),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d') 
        	]);
        }
    }
}
