<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Language::count()==0)
        {
        	DB::table('languages')->insert([
        		'intitule' => 'Français',
                'path_audio' => '/public/audios/french.mp3',
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
                'slug'=>generateSlug()
        	]);

        	DB::table('languages')->insert([
        		'intitule' => 'Biali',
                'path_audio' => '/public/audios/biali.mp3',
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
                'slug'=> generateSlug() 
        	]);

        	DB::table('languages')->insert([
        		'intitule' => 'Bariba',
                'path_audio' => '/public/audios/bariba.mp3',
        		'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
                'slug'=> generateSlug()
        	]);
        }
    }
}
