@extends('layouts.app')

@section('active_partenaires')
   dropdown active
@endsection

@section('breadcrumb')
   PARTENAIRES
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Modifier les informations du partenaire {{$partenaires->intitule}} </h4>

                        <form action="{{'/partenaires/'.$partenaires->slug}}" method="POST" enctype="multipart/form-data">
                           
                            @method('PUT')
                             @csrf

                            <div class="form-group">
                                <label for="intitule">Dénomination</label>
                                <input type="text" name="intitule" class="form-control" id="intitule" value="{{ old('intitule',$partenaires->intitule)}}">
                            </div>
                     
                            <div class="form-group">
                                <label for="description">Description du partenaire</label>
                                <textarea type="text" name="description" class="form-control" id="description">{{ old('description',$partenaires->description)}}
                                 </textarea>
                            </div>

                            <div class="form-group">
                                <label for="zone_intervention">Zone d'intervention</label>
                                <input type="text" name="zone_intervention" value="{{old('zone_intervention',$partenaires->zone_intervention)}}" class="form-control" id="zone_intervention" >
                            </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <img class="card-img" height="250" src="{{old('path_img',$partenaires->path_img)}}" alt="">   
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="" for="path_img">Modifier l'image</label>
                                <input type="file" name="path_img" class="form-control" id="path_img">   
                            </div>
                           </div>
                        </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <audio controls> 
                                    <source src="{{old('path_audio',$partenaires->path_audio)}}" type="">
                                </audio>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="" for="path_audio">Modifier l'audio</label>
                                <input type="file" name="path_audio" class="form-control" id="path_audio">   
                            </div>
                        </div>
                    </div>

                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">Ajouter</button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>



@endsection