@extends('layouts.app')

@section('breadcrumb')
   PARTENAIRES
@endsection

@section('active_partenaires')
   dropdown active
@endsection

@section('content')

    @if($status==1)

<div class="toast" id="closeToast" role="alert" aria-live="assertive" aria-atomic="true" >
    <div class="toast-header">
        {{-- <img src="..." class="rounded mr-2" alt="toast"> --}}
        <strong class="mr-auto">SICAR</strong>
       {{--  <small class="text-muted">11 mins ago</small> --}}
        <button type="button" onclick="fermer()" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">
             Vous avez ajouté un nouveau partenaire
    </div>
</div>

    @endif

    <button type="button" class="btn btn-outline-success float-right" data-toggle="modal" data-target="#formpartenaire" >
                <i class="mdi mdi-plus"></i> Ajouter un nouveau partenaire
    </button>

    <div id="formpartenaire" class="modal fade" role="dialog" aria-labelledby="formcvalabel"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> Ajouter un nouveau partenaire </h5>
                </div>
                    <div class="modal-body">
                        <form action="/partenaires" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('post')

                            <div class="form-group">
                                <label for="intitule">Dénomination</label>
                                <input type="text" name="intitule" class="form-control" id="intitule" required>
                            </div>
                     
                            <div class="form-group">
                                <label for="description">Description du partenaire</label>
                                <textarea type="text" name="description" class="form-control" id="description" required> </textarea>
                            </div>

                            <div class="form-group">
                                <label for="zone_intervention">Zone d'intervention</label>
                                <input type="text" name="zone_intervention" class="form-control" id="zone_intervention" required>
                            </div>

                            <div class="form-group">
                                <label class="" for="path_img">Choisir une image</label>
                                <input type="file" name="path_img" class="form-control" id="path_img">   
                            </div>

                            <div class="form-group">
                                <label class="" for="path_audio">Choisir un audio</label>
                                <input type="file" name="path_audio" class="form-control" id="path_audio">   
                            </div>

                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">Ajouter</button>
                                  <button class="btn btn-outline-danger float-right" type="button" data-dismiss="modal">Annuler</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>

     <div class="row mt-5" style="" >
        @foreach($partenaires as $partenaire)
            <div class="col-md-4 mb-3">
                <div class="card">  
                    <div class="card-body">
                        <a href="{{'/partenaires/'.$partenaire->slug.'/edit'}}" class="float-right btn btn-success">Editer</a>
                         <a class="" href="{{'/partenaires/'.$partenaire->slug}}">
                        <h4 class="card-title">{{$partenaire->intitule}}</h4>
                        <img class="card-img" style="" src="{{$partenaire->path_img}}" >
                        </a>
                    </div>  
               <audio src="{{$partenaire->path_audio}}" controls></audio>                  

                </div>
            </div>
        @endforeach
    </div>
    <br> <br> <br>

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>

       <script type="text/javascript">
       function fermer(){

        var buttonId = document.getElementById('closeToast');
        buttonId.remove();
       } 

    </script>

@endsection
