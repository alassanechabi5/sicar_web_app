@extends('layouts.app')

@section('active_partenaires')
  dropdown active
@endsection

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
    Producteurs du parternaire {{ $partenaire->intitule }}
@endsection

@section('content')

   <div class="row">
        <div class="col-md-12">

            <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Liste des producteurs du partenaire</h4>

                    <table id="tableRelais" class="table table-responsive table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nom et prénom</th>
                                <th>Sexe</th>
                                <th>commune</th>
                                <th>Village</th>
                                <th>Téléphone</th>
                                <th>Whatsapp</th>
                                <th>Langage</th>
                                <th>Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list_relais as $relais)
                                <tr>
                                    <td>{{$relais->full_name}}</td>
                                    <td>{{$relais->sexe}}</td>
                                    <td>{{$relais->commune}}</td>
                                    <td>{{$relais->village}}</td>
                                    <td>{{$relais->phone}}</td>
                                    <td>{{$relais->whatsapp}}</td>
                                    <td>{{$relais->language}}</td>
                                    <td>
                                        <a href="#" class="btn btn-info">Voir</a>
                                    </td>                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </div>
    <br> <br> <br>

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">
        
        $(document).ready(function() {
            var table_relais = $('#tableRelais').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Liste des relais'
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Liste des relais'
                    }
                ],
              

            });
         
            table_relais.buttons().container()
                .appendTo('#tableRelais_wrapper .col-md-6:eq(0)');

        } );
    </script>

@endsection