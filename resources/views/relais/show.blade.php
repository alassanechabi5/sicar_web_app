@extends('layouts.app')

@section('importCss')

  <link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">

  <style type="text/css">

      #playButton {
        display: block;
      }

      #pauseButton {
        display: none;
      }

  </style>
@endsection

@section('breadcrumb')
   Producteurs
@endsection

@section('active_relais')
   dropdown active
@endsection

@section('content')

   <div class="row">
       <div class="col-md-12">
         <div class="card">
           <div class="card-body">
              <h4 class="card-title">Liste des difficultés du producteur </h4>

              <table id="tableDifficultes" class="table table-responsive table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>Producteurs</th>
                      <th>CVA concernée</th>
                      <th>Photo</th>
                      <th>Question du producteur</th>                     
                      <th>Date Réception</th>                               
                    </tr>
                  </thead>

                  <tbody>
                      @foreach($difficultes as $difficulte)
                      <tr>
                          <td>{{$difficulte->relais->full_name}}</td>
                          <td>{{$difficulte->cva->intitule}}</td>
                          <td><img class="" src="{{$difficulte->path_img}}" width="100" height="100"></td>
                          <td>
                            <audio controls src="{{$difficulte->path_audio}}"></audio>
                          </td>                              
                         
                          
                           <td>
                           {{convertirDate($difficulte->created_at)}}
                          </td>

                      </tr>
                      @endforeach

                  </tbody>
              
              </table>
          </div>
          </div>        
      </div>
    </div>
    
        <br><br><br>  

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script> 
   <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>

    <script src="{{asset('/js/recorder/app.js')}}"></script> 

    <script type="text/javascript">
        
        $(document).ready(function() {
            var table_difficultes = $('#tableDifficultes').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                // buttons: [
                //     {
                //         extend: 'excelHtml5',
                //         title: 'Liste des difficultes'
                //     },
                //     {
                //         extend: 'pdfHtml5',
                //         title: 'Liste des difficultes'
                //     }
                // ],
                // "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],

            });
         
            table_difficultes.buttons().container()
                .appendTo('#tableDifficultes_wrapper .col-md-6:eq(0)');
            
        } );


        // function sendDifficultReponse(difficulte_id, relais_id, cva_id)
        // {            
        //     var form_data = new FormData();                         
        //     form_data.append("difficulte_id", difficulte_id);           
        //     form_data.append("relais_id", relais_id);
        //     form_data.append("cva_id", cva_id);
        //     form_data.append("path_audio", file_audio, "reponse_audio.wav");

        //     fetch("/reponseDifficultes", { 
        //                 method: "POST",
        //                 headers: {
        //                     "Access-Control-Origin": "*",
        //                     "X-CSRF-TOKEN": "{{ csrf_token() }}" 
        //                 },               
        //                 body: form_data

        //             }).then(res => res.json() ) 
        //             .then(response => {
        //                 console.log("Réponse recu ", response);

        //                 if(response.result.localeCompare("succes"))
        //                 {
        //                   alert("Reponse envoyée")
        //                 }                        
        //             })
        //             .catch(err => {
        //                 console.log("Oops ! Une erreur s'est produite : ", err)
        //                 alert("Oops ! Une erreur s'est produite : ", err)
        //             });
        // }
  </script>  

@endsection
