@extends('layouts.app')

@section('importCss')

 <link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
    PRODUCTEURS
@endsection

@section('active_relais')
   dropdown active
@endsection

@section('content')

   <div class="row">
        <div class="col-md-12">
                <form action="/relaisRechercher" method="POST" enctype="multipart/form-data">
                    @method('POST')
                    @csrf
                <select class="form-control col-3" name="commune">
                    
                    <option value="all">Toutes les communes</option>
                    @foreach($communes as $one_commune)
                    <option value="{{$one_commune->commune}}">{{$one_commune->commune}}</option>
                    @endforeach
                </select>
       
                {{-- <select class="form-control col-3" name="language" >
                    <option value="">Filtrer par langue</option>
                     @foreach($langues as $langue )
                    <option value="{{$langue->intitule}}">{{$langue->intitule }}</option>
                    @endforeach
                   
                </select> --}}
               

                <select  class="form-control col-3" name="sexe">
                    <option value="all">tous les sexes</option>
                    <option value="Masculin">Masculin</option>
                    <option value="Féminin">Féminin</option>
                </select> 
                <button class="btn btn-primary" type="submit" >Rechercher</button> 
            </form><br><br>
            <div class="card">
            
                  <div class="card-body">
                    <h4 class="card-title">Liste des producteurs</h4>

                    <table id="tableRelais" class="table table-responsive table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nom et prénom</th>
                                <th>Sexe</th>
                                <th>Commune</th>
                                <th>Village</th>
                                <th>Téléphone</th>
                                <th>Whatsapp</th>
                                <th>Langage</th>
                                <th>Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($relais as $one_relais)
                                <tr>
                                    <td>{{$one_relais->full_name}}</td>
                                    <td>{{$one_relais->sexe}}</td>
                                    <td>{{$one_relais->commune}}</td>
                                    <td>{{$one_relais->village}}</td>
                                    <td>{{$one_relais->phone}}</td>
                                    <td>{{$one_relais->whatsapp}}</td>
                                    <td>{{$one_relais->language}}</td>
                                    <td>
                                      <div class="btn-group">
                                        <a href="{{'/relais/'.$one_relais->slug}}" class="btn btn-info btn-fw"><i class="mdi mdi-eye"></i>Voir</a>
                                        <a href="{{'/relais/'.$one_relais->slug.'/edit'}}" class="btn btn-success btn-fw"><i class="mdi mdi-pencil"></i>Editer</a>
                                        @if(Auth::check())
                                        @if(Auth::user()->role_id==1)
                                        <button type="button" class="btn btn-danger btn-fw" data-target="{{'#deleteRelais'.$one_relais->id}}" data-toggle="modal"><i class="mdi mdi-close"></i>Supprimer</button>
                                        @endif
                                        @endif
                                      </div>
                                    </td>                                    
                                </tr>

        {{-- Modal confirmation de suppression d'un producteur --}}

        <div class="modal fade" id="{{'deleteRelais'.$one_relais->id}}" tabindex="-1" role="dialog" aria-labelledby="relaisSuppr" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Voulez vraiment supprimer?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-footer">
                <form action="{{'/relais/'.$one_relais->id}}" method="POST" enctype="multipart/form-data">

                    @method('DELETE')
                    @csrf

                    <button type="submit" class="btn btn-success">Oui</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
              </div>
            </div>
          </div>
        </div>
    {{-- Fin Modal --}}
                            @endforeach
                            
                </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </div>
    <br> <br> <br>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Charger la liste des producteurs</h4>

                    <form name="form" action="{{'/relais-file'}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('post')            

                        <div class="form-group">
                          <label for="csv_file">Choisir le CSV à enregistrer</label>
                            <input type="file" class="form-control" name="csv_file" id="csv_file" required>
                        </div>
                        <br>            
                        
                        <div class="row">
                          <div class="col-3"></div>
                          <div class="col-6">
                            <button type="submit" class="btn btn-outline-success btn-block">
                              <i class="fa fa-save float-left"></i> Charger
                            </button>
                          </div>
                          <div class="col-3"></div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>


    <br> <br> <br>

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">

        $(document).ready(function() {
            var table_relais = $('#tableRelais').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                 buttons: [
                   {
                       extend: 'excelHtml5',
                        title: 'Liste des producteurs'
                   },
                    {
                        extend: 'pdfHtml5',
                        title: 'Liste des producteurs'
                    }
                ],
                 "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],

            });
         
            table_relais.buttons().container()
                .appendTo('#tableRelais_wrapper .col-md-6:eq(0)');
            
        } );

//     $(document).ready(function() {
//     $('#tableRelais').DataTable( {

//         "dom": '<"toolbar">', //This is used to create an toolbar dom element

//         initComplete: function () {
//             this.api().columns([2,3,7], {selected:true}).every( function () {
//                 var column = this; //Retrieve the current columns in the array [2,3,7]
//                 var texte = '';
//                 texte = 'Filtrer par ' + $(column.header()).html();

//                 var select = $('<select class=""><option value="">'+texte+'</option></select>')
//                     .appendTo($("div.toolbar"))
//                     .on( 'change', function () {
//                         var val = $.fn.dataTable.util.escapeRegex(
//                             $(this).val()
//                         );
//                         column
//                             .search( val ? '^'+val+'$' : '', true, false )
//                             .draw();
//                     } ); 
//                 column.data().unique().sort().each( function ( d, j ) {
//                     select.attr("class",'form-control form-control-md col-3')
//                     select.append( '<option value="'+d+'">'+d+'</option>' )
//                 } );
//             } );
//         }
//     } );
// } )

    </script>

@endsection