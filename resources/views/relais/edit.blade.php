@extends('layouts.app')

@section('active_relais')
   dropdown active
@endsection

@section('breadcrumb')
   PRODUCTEURS
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Modifier les informations du producteur {{ $relais->full_name }} </h4>

                        <form action="{{'/relais/'.$relais->slug}}" method="POST" enctype="multipart/form-data">
                           
                            @method('PUT')
                             @csrf
                    <blockquote class="blockquote blockquote-muted">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="full_name">Dénomination</label>
                                <input type="text" name="full_name" class="form-control" id="full_name" value="{{ old('full_name',$relais->full_name)}}">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="partenaire_id">Partenaire</label>
                                <select name="partenaire_id" class="form-control" id="partenaire_id">
                                @foreach($partenaires as $partenaire)
                                    <option value="{{$partenaire->id}}">{{$partenaire->intitule}}</option>
                                @endforeach
                                </select>
                            </div>
                          </div>
                        </div>
                                         
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="ethnic_group">Groupe ethnique</label>
                                <input type="text" name="ethnic_group" class="form-control" id="ethnic_group" value="{{ old('ethnic_group',$relais->ethnic_group)}}">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="sexe">Sexe</label>
                                <select name="sexe" class="form-control" id="sexe">
                                    <option value="Masculin">Masculin</option>
                                    <option value="Feminin">Feminin</option>
                                </select>
                            </div>
                           </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="commune">Commune</label>
                                <input type="text" name="commune" class="form-control" id="commune" value="{{ old('commune',$relais->commune)}}">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="village">Village</label>
                                <input type="text" name="village" class="form-control" id="village" value="{{ old('village',$relais->village)}}">
                            </div>
                          </div>
                        </div>
                    </blockquote>

                    <blockquote class="blockquote blockquote-muted">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Téléphone</label>
                                <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone',$relais->phone)}}">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="whatsapp">Whatsapp</label>
                                <input type="text" name="whatsapp" class="form-control" id="whatsapp" value="{{ old('whatsapp',$relais->whatsapp)}}">
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="age">Age</label>
                                <input type="text" name="age" class="form-control" id="age" value="{{ old('age',$relais->age)}}">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="language">Langue</label>
                                <select name="language" class="form-control" id="language">
                                @foreach($langages as $langage)
                                    <option value="{{$langage->intitule}}">{{$langage->intitule}}</option>
                                @endforeach
                                </select>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="taille_menage">Taille ménage</label>
                                <input type="text" name="taille_menage" class="form-control" id="taille_menage" value="{{ old('taille_menage',$relais->taille_menage)}}">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="actif_agricole">Actif agricole</label>
                                <input type="text" name="actif_agricole" class="form-control" id="actif_agricole" value="{{ old('actif_agricole',$relais->actif_agricole)}}">
                            </div>
                          </div>
                        </div>

                    <div class="form-group">
                        <label for="respo">Responsable</label>
                        <input type="text" name="respo" class="form-control" id="respo" value="{{ old('respo',$relais->respo)}}">
                    </div>
                    </blockquote>

                    <blockquote class="blockquote blockquote-muted">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <img class="" height="150" width="150" src="{{old('avatar',$relais->avatar)}}" alt="">   
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="" for="avatar">Modifier l'image</label>
                                <input type="file" name="avatar" class="form-control" id="avatar">   
                            </div>
                           </div>
                        </div>
                    </blockquote>


                            <div class="form-group">
                                  <button type="submit" class="btn btn-success btn-block">Modifier</button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>



@endsection