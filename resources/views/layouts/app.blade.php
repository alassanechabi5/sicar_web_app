<!DOCTYPE html>
<html lang="fr">
  <head>



    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SICAR</title>
    
    <link rel="stylesheet" href="{{asset('/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/vendors/iconfonts/ionicons/dist/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{asset('/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('/vendors/css/vendor.bundle.addons.css')}}">
   
    <link rel="stylesheet" href="{{asset('/css/shared/style.css')}}">
    
    <link rel="stylesheet" href="{{asset('/css/demo_1/style.css')}}">
    
    <link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" />
    
    @yield('importCss')

  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="#">
           <img  src="{{asset('/images/log.jpg')}}" height="56px" style="margin-right: 220px" /> 
           </a>
         
          {{-- <a class="navbar-brand brand-logo-mini" href="#">
            <img src="{{asset('/images/logo-rbt-wap.png')}}" alt="logo" /> </a> --}}
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
          <ul class="navbar-nav">
            <li class="nav-item font-weight-semibold d-none d-lg-block">
              @yield('breadcrumb')
            </li>            
          </ul>
          
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <img class="img-xs rounded-circle" src="{{Auth::user()->avatar}}" alt="Profile image">&nbsp;&nbsp; {{Auth::user()->full_name}}</a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                  <img class="img-md rounded-circle" src="{{Auth::user()->avatar}}" alt="Profile image">
                  <p class="mb-1 mt-3 font-weight-semibold">
                    {{Auth::user()->full_name}}
                  </p>
                  <p class="font-weight-light text-muted mb-0">{{Auth::user()->email}}</p>
                </div>
                <a href="{{'/users/'.Auth::user()->slug}}" class="dropdown-item">Mon profil <i class="dropdown-item-icon ti-dashboard"></i></a>
                
                <a href="{{'/users/'.Auth::user()->slug.'/edit'}}" class="dropdown-item">Editer mon profil<i class="dropdown-item-icon ti-comment-alt"></i></a>
                <a href="/users/create" class="dropdown-item">Ajouter un nouvel utilisateur<i class="dropdown-item-icon ti-location-arrow"></i></a>
                <a href="/languages" class="dropdown-item">Langues<i class="dropdown-item-icon ti-location-arrow"></i></a>
                
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    Déconnexion <i class="dropdown-item-icon ti-power-off"></i>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>

              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
              
              <li class="nav-item @yield('active_home')">
              <a class="nav-link" href="/home">
                <i class="mdi mdi-poll" style="font-size: 1.2rem;"></i>
                <span class="menu-title">  Tableau de bord</span>
              </a>
            </li>

            <li class="nav-item @yield('active_modules')">
              <a class="nav-link" href="/modules">
                <i class="mdi mdi-folder" style="font-size: 1.2rem;"></i>
                <span class="menu-title">Module </span>
              </a>
            </li>
            <li class="nav-item @yield('active_formation')">
              <a class="nav-link"  href="/formations" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-animation" style="font-size: 1.2rem;"></i>
                <span class="menu-title">Cours suivis</span>
              </a>
            </li>            
            <li class="nav-item @yield('active_difficultes')">
              <a class="nav-link" href="/difficultes">
                <i class="mdi mdi-forum" style="font-size: 1.2rem;"></i>
                <span class="menu-title">Difficultés</span>
              </a>
            </li>
            <li class="nav-item @yield('active_relais')">
              <a class="nav-link" href="/relais">
                <i class="mdi mdi-account-convert" style="font-size: 1.2rem;"></i>
                <span class="menu-title">Producteurs</span>
              </a>
            </li>
            <li class="nav-item @yield('active_partenaires')">
              <a class="nav-link" href="/partenaires">
                <i class="mdi mdi-account-switch" style="font-size: 1.2rem;"></i> 
                <span class="menu-title">Partenaires</span>
              </a>
            </li>
            <li class="nav-item @yield('active_users')">
              <a class="nav-link" href="/users">
                <i class="mdi mdi-account-outline" style="font-size: 1.2rem;"></i>
                <span class="menu-title">Utilisateurs</span>
              </a>
            </li>

          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            
            @yield('content')

          </div>


          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2021</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>

    <script src="{{asset('/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('/vendors/js/vendor.bundle.addons.js')}}"></script>    
    <script src="{{asset('/js/shared/misc.js')}}"></script>
    <script src="{{asset('/js/shared/off-canvas.js')}}"></script>


    @yield('importJs')
    
  </body>
</html>