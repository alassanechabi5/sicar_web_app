@extends('layouts.app')

@section('breadcrumb')
   LANGUAGE
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Modifier la langue</h4>

                        <form action="{{'/languages/'.$langues->slug}}" method="POST" enctype="multipart/form-data">
                           
                            @method('PUT')
                             @csrf

                            <div class="form-group">
                                <label for="intitule">Intitulé de la langue</label>
                                <input type="text" name="intitule" class="form-control" id="intitule" value="{{ old('intitule',$langues->intitule)}}" required>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <audio controls>
                                    <source src="{{$langues->path_audio}}">
                                  </audio>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label for="path_audio">Choisir un nouvel audio</label>
                                    <input type="file" name="path_audio" class="form-control{{ $errors->has('path_audio') ? ' is-invalid' : '' }}" id="path_audio" name="path_audio"> 
                                      @if ($errors->has('path_audio'))
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('path_audio') }}</strong>
                                        </span>
                                      @endif   
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">Valider</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>



@endsection