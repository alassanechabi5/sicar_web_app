@extends('layouts.app')

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
    LANGUES
@endsection

{{-- @section('active')
   dropdown active
@endsection --}}

@section('content')
 
 <button type="button" class="btn btn-outline-success float-right" data-toggle="modal" data-target="#formlanguage" >
                <i class="mdi mdi-plus"></i> Ajouter une nouvelle Langue
</button>

<div id="formlanguage" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="formlanglabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> Ajouter une nouvelle langue</h5>
                </div>
                    <div class="modal-body">
                        <form action="/languages" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('post')

                            <div class="form-group">
                                <label for="intitule">Intitulé de la langue</label>
                                <input type="text" name="intitule" class="form-control" id="intitule" required >
                            </div>

                            <div class="form-group">
                                <label class="" for="path_audio">Choisir un audio</label>
                                <input type="file" name="path_audio" class="form-control{{ $errors->has('path_audio') ? ' is-invalid' : '' }}" id="path_audio" name="path_audio"> 
                                 @if ($errors->has('path_audio'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('path_audio') }}</strong>
                                </span>
                            @endif                                
                            </div>

                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">Ajouter</button>
                                  <button class="btn btn-outline-danger float-right" type="button" data-dismiss="modal">Annuler</button>
                            </div>
                        </form>
                    </div>
                </div>
    </div>
</div>

   <div class="row mt-5">
        <div class="col-md-12">

            <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Liste des langues</h4>
                     @if ($errors->has('path_audio'))
                        <span class="alert alert-warning" role="alert">
                            <strong>{{ "Erreur: langage non ajouté" }}</strong>
                        </span>
                     @endif 


                    <table id="tableLangues" class="table table-responsive table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Langues</th>
                                <th>Audio</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($langues as $langue)
                                <tr>
                                    <td>{{$langue->intitule}}</td>
                                    <td>
                                        <audio controls>
                                            <source src="{{$langue->path_audio}}">
                                        </audio>
                                    </td>
                                    <td>
                                      <div class="btn-group">
                                        <a href="{{'/languages/'.$langue->slug.'/edit'}}" class="btn btn-success btn-fw"><i class="mdi mdi-pencil"></i>Editer</a>
                                        <button type="button" class="btn btn-danger btn-fw" data-target="{{'#deleteLangue'.$langue->id}}" data-toggle="modal"><i class="mdi mdi-close"></i>Supprimer</button>
                                      </div>
                                    </td>                                    
                                </tr>

        {{-- Modal confirmation de suppression d'un producteur --}}

        <div class="modal fade" id="{{'deleteLangue'.$langue->id}}" tabindex="-1" role="dialog" aria-labelledby="langueSuppr" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Voulez vraiment supprimer?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-footer">
                <form action="{{'/languages/'.$langue->id}}" method="POST" enctype="multipart/form-data">

                    @method('DELETE')
                    @csrf

                    <button type="submit" class="btn btn-success">Oui</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
              </div>
            </div>
          </div>
        </div>
    {{-- Fin Modal --}}
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </div>
    
@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">
        
        $(document).ready(function() {
            var table_langues = $('#tableLangues').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                });
         
            table_langues.buttons().container()
                .appendTo('#tableLangues_wrapper .col-md-6:eq(0)');

        } );
    </script>

@endsection