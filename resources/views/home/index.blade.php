@extends('layouts.app')

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
   DASHBOARD
@endsection

@section('active_home')
   dropdown active
@endsection

@section('content')

   <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Nombre de producteurs par commune</h4>
                    
                      <table class="table table-responsive table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Commune</th>
                            <th>Nombre de producteurs</th>                            
                          </tr>
                         </thead>

                        <tbody>
                          @foreach($relais_commune as $key => $one_relais)

                            <tr>
                              <td>
                                <p class="mb-1 text-dark font-weight-medium">{{$one_relais->commune}}</p>
                              </td>
                              <td class="font-weight-medium">{{$one_relais->compte}}</td>
                            </tr>   
                          @endforeach                       
                        </tbody>
                      </table>
                   
                  </div>
                </div>
              </div>

       <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Nombre de Producteurs ayant suivi de formation</h4>
                    
                      <table class="table table-responsive table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Nom de la CVA</th>
                            <th>Nombre de Producteurs</th>
                           
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($cours_suivis as $cours_suivi)

{{--                             @foreach($relais_formation as $tous_relais)
 --}}                          <tr>
                             <td> {{$cours_suivi->module->intitule}}</td>
                            
                            <td class="font-weight-medium">{{$cours_suivi->relais_id}}</td>
                          </tr>
{{--                           @endforeach
 --}}                          @endforeach
                        </tbody>
                      </table>
                   
                  </div>
                </div>
              </div>

               <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                  <div class="p-4 border-bottom bg-light">
                    <h4 class="card-title mb-0">Histogramme des producteurs par commune</h4>
                  </div>
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center pb-4">
                     {{--  <h4 class="card-title mb-0">Sales Revenue</h4> --}}
                      <div id="bar-traffic-legend"></div>
                    </div>
                    {{-- <p class="mb-4">17% increase in sales than last week</p> --}}
                    <canvas id="barChart" style="height:250px"></canvas>
                  </div>
                </div>
              </div>

               <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                  <div class="p-4 border-bottom bg-light">
                    <h4 class="card-title mb-0">Histogramme des producteurs ayant suivi de formation</h4>
                  </div>
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center pb-4">
                      {{-- <h4 class="card-title mb-0">Users by Device</h4> --}}
                      <div id="stacked-bar-traffic-legend"></div>
                    </div>
                    {{-- <p class="mb-4">25% more traffic than previous week</p> --}}
                    <canvas id="stackedbarChart" style="height:250px"></canvas>
                  </div>
                </div>
              </div>
    </div>

@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/shared/chart.js')}}"></script>
@endsection

@endsection
