@extends('layouts.app')

@section('active_modules')
   dropdown active
@endsection

@section('logo')
  <img class="card-img" height="50"  src="images/log.jpg"/> 
@endsection

@section('breadcrumb')
   MODULES
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Modifier les informations de la CVA</h4>

                        <form action="{{'/cvas/'.$cvas->slug}}" method="POST" enctype="multipart/form-data">
                           
                            @method('PUT')
                             @csrf

                            <div class="form-group">
                                <label for="intitule">Dénomination</label>
                                <input type="text" name="intitule" class="form-control" id="intitule" value="{{ old('intitule',$cvas->intitule)}}">
                            </div>
                     
                            <div class="form-group">
                                <label for="description">Description de la CVA</label>
                                <textarea type="text" name="description" class="form-control" id="description">{{ old('description',$cvas->description)}}
                                 </textarea>
                            </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <img class="card-img" height="250" src="{{$cvas->path_img}}" alt="">   
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="" for="path_img">Choisir une image</label>
                                <input type="file" name="path_img" class="form-control" id="path_img">   
                            </div>
                          </div>
                        </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">Ajouter</button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>

@endsection