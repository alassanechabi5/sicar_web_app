@extends('layouts.app')

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
   FORMATIONS
@endsection

@section('active_formation')
   dropdown active
@endsection

@section('content')

   <div class="row">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Liste des formations</h4>

                    <table id="tableFormations" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Producteurs</th>
                                <th>Module</th>
                                <th>Note d'évaluation</th>
                                <th>Action</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($formations as $formation)
                            @foreach($formationsM as $formationm)
                                <tr>
                                    <td>{{$formation->relais->full_name}}</td>
                                    <td>{{$formationm->module->num_chapitre}}</td>
                                    <td>{{$formation->note_evaluation}}</td>

                                   <!-- <audio src=""></audio>-->
                                   
                                    <td>
                                        <a href="#" class="btn btn-info">Voir</a>
                                    </td>                                    
                                </tr>
                            @endforeach
                        @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </div>
    <br> <br> <br>

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">
        
        $(document).ready(function() {
            var table_formations = $('#tableFormations').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Liste des formations'
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Liste des formations'
                    }
                ],
              

            });
         
            table_formations.buttons().container()
                .appendTo('#tableFormation_wrapper .col-md-6:eq(0)');

        } );
    </script>

@endsection