@extends('layouts.app')

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
   Nouvel Utilisateur
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Ajouter un nouvel utilisateur</h4>
                   <form action="/users" method="POST" class="form-sample" enctype="multipart/form-data">
                   	@csrf
                   	@method('post')

                   	<div class="row">
                   	  <div class="col-md-6">
                          <label for="full_name">Nom et Prénoms</label>
                   		<div class="form-group">
                        			<input type="text" class="form-control" id="full_name" name="full_name">
                      	</div>
                      </div>
                      <div class="col-md-6">
                      	<div class="form-group">
                        	<label for="email">Adresse mail</label>
                        		
                        			<input type="email" class="form-control" id="email" name="email">
                        		
                      	</div>
                  	  </div>
                  	</div>

                     <div class="row">
                   		<div class="col-md-6">
                   			<div class="form-group">
                        <label for="country">Pays</label>
                        <input type="text" class="form-control" id="country" name="country">
                      </div>
                  </div>

              		<div class="col-md-6">
                      <div class="form-group">
                        <label for="town">Ville</label>
                         
                        <input type="text" class="form-control" id="town" name="town">
                    </div>
                      </div>
              </div>


              <div class="row">
                   		<div class="col-md-6">
                   			<div class="form-group ">
                        <label for="phone1">Contact 1</label>
                       
                        <input type="text" class="form-control" id="phone1" name="phone1">
                  </div>
              </div>
              		<div class="col-md-6">
                       <div class="form-group ">
                        <label for="phone2">Contact 2</label>
                        
                        <input type="text" class="form-control" id="phone2" name="phone2">
                      </div>
                        </div>
              </div>


              <div class="row">
                      <div class="col-md-6">
                        <div class="form-group ">
                        <label for="partenaire_id">Partenaire</label>
                       
                        <select class="form-control" name="partenaire_id" id="partenaire_id">
                          @foreach($partenaires as $one_partenaire)
                            <option value="{{$one_partenaire->id}}">{{$one_partenaire->intitule}}</option>
                          @endforeach
                        </select>
                  </div>
              </div>

                  <div class="col-md-6">
                       <div class="form-group ">
                        <label for="role_id">Rôle</label>
                        
                    <select class="form-control" name="role_id" id="role_id">
                      @foreach($roles as $one_role)
                          <option value="{{$one_role->id}}"> {{$one_role->intitule}} </option>
                      @endforeach
                    </select>
                      </div>
                        </div>
              </div>

 					 <div class="row">
                   		<div class="col-md-6">
                   			<div class="form-group">
                        <label for="identifiant">Identifiant</label>
                       
                        <input type="text" class="form-control" id="identifiant" name="identifiant">
                      </div>
                  </div>

                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="password">Mot de passe</label>
                        
                        <input type="password" class="form-control" id="password" name="password">
                      </div>
                     </div>
                 </div>

                      <div class="form-group">
                        <label for="avatar" >Image de profil</label>
                        <input type="file" id="avatar" name="avatar">
                      </div>   
                      <div class="form-group">                 
                      <button type="submit" class="btn btn-success mr-2">{{ __("Enregistrer") }}</button>
                      </div>
                 </form>

                  </div>
                </div>
              </div>
          </div>

@endsection