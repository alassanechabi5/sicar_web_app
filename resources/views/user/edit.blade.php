@extends('layouts.app')

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
   Modifier
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Modifier mon profil</h4>
                   <form action="{{'/users/'.$user->slug}}" method="POST" class="form-sample" enctype="multipart/form-data">
                   	@csrf
                   	@method('PUT')

                   	<div class="row">
                   	  <div class="col-md-6">
                   		<div class="form-group">
                        	<label for="full_name">Nom et Prénoms</label>
                        			<input type="text" class="form-control" id="full_name" name="full_name" value="{{ old('full_name',$user->full_name)}}">
                      	</div>
                      </div>
                      <div class="col-md-6">
                      	<div class="form-group">
                        	<label for="email">Adresse mail</label>
                        		
                        			<input type="email" class="form-control" id="email" name="email" value="{{ old('email',$user->email)}}">
                        		
                      	</div>
                  	  </div>
                  	</div>

                     <div class="row">
                   		<div class="col-md-6">
                   			<div class="form-group">
                        <label for="country">Pays</label>
                        <input type="text" class="form-control" id="country" name="country" value="{{ old('country',$user->country)}}">
                      </div>
                  </div>

              		<div class="col-md-6">
                      <div class="form-group">
                        <label for="town">Ville</label>
                         
                        <input type="text" class="form-control" id="town" name="town" value="{{ old('town',$user->town)}}">
                    </div>
                      </div>
              </div>


              <div class="row">
                   		<div class="col-md-6">
                   			<div class="form-group ">
                        <label for="phone1">Contact 1</label>
                       
                        <input type="text" class="form-control" id="phone1" name="phone1" value="{{ old('phone1',$user->phone1)}}">
                  </div>
              </div>
              		<div class="col-md-6">
                       <div class="form-group ">
                        <label for="phone2">Contact 2</label>
                        
                        <input type="text" class="form-control" id="phone2" name="phone2"  value="{{ old('phone2',$user->phone2)}}">
                      </div>
                        </div>
              </div>


              <div class="row">
                      <div class="col-md-6">
                        <div class="form-group ">
                        <label for="partenaire_id">Partenaire</label>
                       
                        <select class="form-control" name="partenaire_id" id="partenaire_id">
                          @foreach($partenaires as $one_partenaire)
                            <option value="{{$one_partenaire->id}}">{{$one_partenaire->intitule}}</option>
                          @endforeach
                        </select>
                  </div>
              </div>
             @if(Auth::check())
             @if(Auth::user()->role_id == 1)
                  <div class="col-md-6">
                       <div class="form-group ">
                        <label for="role_id">Rôle</label>
                        
                    <select class="form-control" name="role_id" id="role_id">
                      @foreach($roles as $one_role)
                          <option value="{{$one_role->id}}"> {{$one_role->intitule}} </option>
                      @endforeach
                    </select>
                      </div>
                        </div>
                        @endif
                        @endif
              </div>

 					 <div class="row">
                   		<div class="col-md-6">
                   			<div class="form-group">
                        <label for="identifiant">Identifiant</label>
                       
                        <input type="text" class="form-control" id="identifiant" name="identifiant" value="{{ old('identifiant',$user->identifiant)}}" >
                      </div>
                  </div>

                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="password">Mot de passe</label>
                        
                        <input type="password" class="form-control" id="password" name="password" value="">
                      </div>
                     </div>
                 </div>

                 <div class="row">
                 <div class="col-md-6">
                   
                   <img src="{{$user->avatar}}" height="100px">
                 </div> 

                      <div class="col-md-6">
                        <label for="avatar" >Image de profil</label>
                        <input type="file" id="avatar" name="avatar" class="form-control">
                      </div>
                      </div> <br> <br> <br>                   
                      <button type="submit" class="btn btn-success mr-2">Modifier</button>
                 </form>

                  </div>
                </div>
              </div>
          </div>

@endsection