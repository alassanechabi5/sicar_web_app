@extends('layouts.app')

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
    UTILISATEURS
@endsection


@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Liste des utilisateurs</h4>

                    <table id="tableUsers" class="table table-responsive table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Role</th>
                                <th>Nom</th>
                                <th>Identifiant</th>
                                <th>Email</th>
                                <th>Pays</th>
                                <th>Ville</th>
                                <th>Téléphone</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->role->intitule}}</td>
                                    <td>{{$user->full_name}}</td>
                                    <td>{{$user->identifiant}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->country}}</td>
                                    <td>{{$user->town}}</td>
                                    <td>{{$user->phone1}} <br> {{$user->phone2}}</td>
                                    <td>
                                        <a href="#" class="btn btn-info">Voir</a>
                                    </td>                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </div>
    <br> <br> <br>

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">
        
        $(document).ready(function() {
            var table_users = $('#tableUsers').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Liste des utilisateurs'
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Liste des utilisateurs'
                    }
                ],
                // "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],

            });
         
            table_users.buttons().container()
                .appendTo('#tableUsers_wrapper .col-md-6:eq(0)');

            /*$('#tableUsers_wrapper').addClass('row');
            document.querySelector('.dt-buttons').style.height = "50%";
            $('.dataTables_length').addClass('col-md-3');
            document.querySelector('.dataTables_length').style.marginLeft = "10%";
            $('.dataTables_filter').addClass('col-md-6');*/
        } );
    </script>  


@endsection



   
    