
@extends('layouts.app')

@section('content')
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Your Profile</h4>
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form>
                              <div class="form-group row">
                                <label for="identifiant" class="col-4 col-form-label">Identifiant</label> 

                                {{$user->identifiant}}
                                
                              </div>
                              <div class="form-group row">
                                <label for="full_name" class="col-4 col-form-label">Nom et Prénoms</label> 
                                {{$user->full_name}}
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-4 col-form-label">Email</label>
                                {{$user->email}} 
                              </div>
                              <div class="form-group row">
                                <label for="country" class="col-4 col-form-label">Pays</label> 
                                {{$user->country}}
                                
                              </div>
                              <div class="form-group row">
                                <label for="town" class="col-4 col-form-label">Ville</label>
                                {{$user->town}} 
                                
                              </div>
                              <div class="form-group row">
                                <label for="phone1" class="col-4 col-form-label">Téléphone 1</label>
                                {{$user->phone1}} 
                                
                              </div>
                              <div class="form-group row">
                                <label for="phone2" class="col-4 col-form-label">Téléphone 2</label> 
                                {{$user->phone2}}
                               
                              </div>

                              <div class="form-group row">
                                <label for="phone2" class="col-4 col-form-label">Photo </label> 
                               <img src="{{$user->avatar}}" height="100px"> 
                               
                              </div>
                            
                            
                              <div class="form-group row">
                                <div class="offset-4 col-8">
                               <a href="{{'/users/'.$user->slug.'/edit'}}" class="btn btn-primary">Modifier</a>
                                </div>
                              </div>
                            </form>
		                </div>
		            </div>
		            
		        </div>
		    </div>
		</div>

    @endsection
