
@extends('layouts.app')

@section('active_modules')
   dropdown active
@endsection

@section('breadcrumb')
   MODULES
@endsection

@section('content')

<div class="row mt-4" style="justify-content: center;">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Modifier les informations du module<h4>

        <form action="{{ '/modules/'.$module->slug }}" method="POST" enctype="multipart/form-data">
          @method('PUT')
          @csrf

          <blockquote class="blockquote blockquote-muted">
            <div id="form1">
              <span class="float-right">Etape 1/4</span>
              <input type="text" name="slug" id="slug" value="{{ $module->slug }}" hidden>

              <div class="form-group">
                <label for="cva_id">Nom de la CVA</label>
                <input type="text" disabled name="cva_id" class="form-control" id="cva_id" value="{{ old('cva_id',$module->cva->intitule)}}">
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="num_chapitre">Numéro du Chapitre</label>
                    <input type="text" name="num_chapitre" class="form-control" id="num_chapitre" value="{{ old('num_chapitre',$module->num_chapitre)}}">
                  </div>
                </div>
                  
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="language_id">Langue</label>
                      <select id="language_id" name="language_id" class="form-control">
                        @foreach($langues as $langue)
                        <option value="{{$langue->id}}">{{$langue->intitule}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="intitule">Intitulé du module</label>
                      <input type="text" name="intitule" class="form-control" id="intitule" value="{{ old('intitule',$module->intitule)}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea type="text" name="description" class="form-control" id="description"> {{ old('description',$module->description)}}</textarea>
                  </div>
                </div>
              </div>
            </div> 
            {{-- End form 1 --}}

        @php
          $images = $module->path_img;
          $tab_images = explode(";", $images);
          $taille_img = count($tab_images);

          $audios = $module->path_audio;
          $tab_audios = explode(";", $audios);
          $taille_audio = count($tab_audios);

          $videos = $module->path_video;
          $tab_videos = explode(";", $videos);
          $taille_video = count($tab_videos);


        @endphp

            <div id="form2" hidden>
              <span class="float-right">Etape 2/4</span>
              @for($l=0;$l<$taille_img-1;$l++)
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <img height="150" src="{{$tab_images[$l]}}">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="path_img[]">Modifier Image</label>
                      <input type="file" name="{{'path_img[]'}}" class="form-control{{ $errors->has('path_img') ? ' is-invalid' : '' }} {{ $errors->has('path_img.*') ? ' is-invalid' : '' }}">
                      @if ($errors->has('path_img'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('path_img') }}</strong>
                        </span>
                     @endif

                      @if ($errors->has('path_img.*'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('path_img.*') }}</strong>
                        </span>
                      @endif  
                  </div>
                </div>

              </div>            
            @endfor
            
            <div id="newFieldsPhoto"> </div>

                <button class="btn btn-outline-primary" type="button" onclick="addFieldImage()">Nouvelle image</button>
                <button class="btn btn-outline-danger" type="button" onclick="deleteFieldImage()">Supprimer l'image</button>
            </div> 
            {{-- End form 2 --}}

            <div id="form3" hidden>
              <span class="float-right">Etape 3/4</span>
                @for($m=0;$m<$taille_audio-1;$m++)
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <audio controls>
                          <source src="{{$tab_audios[$m]}}">
                        </audio>                              
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="path_audio[]">Modifier l'audio</label>
                        <input type="file" id="path_audio[]" name="{{'path_audio[]'}}" class="form-control{{ $errors->has('path_audio') ? ' is-invalid' : '' }} {{ $errors->has('path_audio.*') ? ' is-invalid' : '' }}"> 

                       @if ($errors->has('path_audio'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('path_audio') }}</strong>
                              </span>
                        @endif
                        @if ($errors->has('path_audio.*'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('path_audio.*') }}</strong>
                              </span>
                        @endif
                      </div>
                    </div>
                  </div> 
                @endfor

            <div id="newFieldsAudio"> </div>
            
              <button class="btn btn-outline-primary" type="button" onclick="addFieldAudio()">Nouveau audio</button>
              <button class="btn btn-outline-danger" type="button" onclick="deleteFieldAudio()" >Supprimer l'audio</button>
          </div> 
          {{-- End form 3 --}}

          <div id="form4" hidden>
            <span class="float-right">Etape 4/4</span>
              @for($n=0;$n<$taille_video-1;$n++)
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <video height="150" controls src="{{$tab_videos[$n]}}">
                      </video>                              
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="path_video[]">Modifier la vidéo</label>
                      <input id="path_video[]" type="file" name="{{'path_video[]'}}" class="form-control{{ $errors->has('path_video') ? ' is-invalid' : '' }} {{ $errors->has('path_video.*') ? ' is-invalid' : '' }}">

                     @if ($errors->has('path_video'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('path_video') }}</strong>
                                  </span>
                      @endif 
                      @if ($errors->has('path_video.*'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('path_video.*') }}</strong>
                                  </span>
                      @endif
                    </div>
                  </div>
                </div> 
              @endfor
          
          <div id="newFieldsVideo"> </div>

            <button class="btn btn-outline-primary" type="button" onclick="addFieldVideo()">Nouvelle vidéo</button>
            <button class="btn btn-outline-danger" type="button" onclick="deleteFieldVideo()" >Supprimer la vidéo</button>
        </div>
            {{-- End Form 4 --}}


          </blockquote>

            <div class="form-group">
                  <button type="button" id="nextButton" class="btn btn-inverse-success float-right">Suivant</button>
                  <button type="button" id="prevButton" class="btn btn-inverse-primary float-left" hidden>Précédent</button>
                  <button hidden id="confirmButton" type="submit" class="btn btn-inverse-success float-right">Valider</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection


@section('importJs')

<script src="{{asset('/js/jquery-3.5.1.js')}}"></script>

<script type="text/javascript">

  var formulaireaffiche= "";
  var formulairecache ="";
  var currentform = 1;
  var afficher = currentform;
  var cacher = 0;

  // showForm(afficher,cacher,currentform);

  function showForm(a,c,p)
  {
    formulaireaffiche= "form"+a;
    formulairecache = "form"+c;

      document.getElementById(formulaireaffiche).removeAttribute("hidden");
      document.getElementById(formulairecache).setAttribute("hidden",true);
 
    if(p<=1){

          document.getElementById('prevButton').setAttribute("hidden",true);
          document.getElementById('confirmButton').setAttribute("hidden",true);
          document.getElementById('nextButton').removeAttribute("hidden");

      }else if((p>1)||(p<4)){
          document.getElementById('prevButton').removeAttribute("hidden");
          document.getElementById('confirmButton').setAttribute("hidden",true);
          document.getElementById('nextButton').removeAttribute("hidden");

    }if(p===4){
      document.getElementById('prevButton').removeAttribute("hidden");
      document.getElementById('confirmButton').removeAttribute("hidden");
      document.getElementById('nextButton').setAttribute("hidden",true);
    }

  }

  nextButton.onclick = () =>{  

    if (currentform === 4) {
      afficher = 4;
      cacher = currentform -1;
      currentform = currentform;
      showForm(afficher,cacher,currentform);
    }else{
      cacher = currentform;
      afficher = currentform +1;
      currentform++;
      showForm(afficher,cacher,currentform);
    }
  }

  prevButton.onclick = () =>{

      if (currentform<=1) {
        cacher = 0;
        afficher = currentform;
        currentform--;
        showForm(afficher,cacher,currentform);
      }else{
      cacher = currentform;
      afficher = currentform -1;
      currentform--;
      showForm(afficher,cacher,currentform);
    }          
}
</script>

<script type="text/javascript">
    
    var positionImage = 0;
    var positionAudio = 0;
    var positionVideo = 0;

    /*Ajouter champ image*/
        function addFieldImage()
        {
          positionImage+=1;
          var champsImage ="<div class='form-group' id='ligneimg"+positionImage+"'><label  for='path_images'> Sélectionnez une image</label><input type='file' id='path_images' name='path_images[]' class='form-control'> <br><br> </div>";

          var ajout = $("#newFieldsPhoto");

          ajout.append(champsImage);
         }

    /*Ajouter champ audio*/
         function addFieldAudio()
        {
           positionAudio+=1;

          var champsAudio ="<div class='form-group' id='ligneaud"+positionAudio+"'><label  for='path_audios'> Sélectionnez un audio</label><input type='file' id='path_audios' name='path_audios[]' class='form-control'> <br><br></div>";

          var ajout = $("#newFieldsAudio");

          ajout.append(champsAudio);
         }

         /*Ajouter champ video*/
         function addFieldVideo()
        {
          positionVideo+=1;
          var champsVideo ="<div class='form-group' id='lignevid"+positionVideo+"'><label  for='path_videos'> Insérez un lien vidéo</label><input type='file' id='path_videos' name='path_videos[]' class='form-control'> <br><br></div>";

          var ajout = $("#newFieldsVideo");

          ajout.append(champsVideo);
         }

         /*Supprimer champ image*/
         function deleteFieldImage()
        {
          if (positionImage > 0) {

            let ligne_id = "#ligneimg"+positionImage;
            var ligne_image = $(ligne_id);
            ligne_image.remove();

            positionImage -=1;
          }
         }

         /*Supprimer champ audio*/
         function deleteFieldAudio()
        {
          if (positionAudio > 0) {

            let ligne_id = "#ligneaud"+positionAudio;
            var ligne_audio = $(ligne_id);
            ligne_audio.remove();

            positionAudio -=1;
          }
         }

         /*Supprimer champ vidéo*/
         function deleteFieldVideo()
        {
          if (positionVideo > 0) {

            let ligne_id = "#lignevid"+positionVideo;
            var ligne_video = $(ligne_id);
            ligne_video.remove();

            positionVideo -=1;
          }
         }
</script>


@endsection