@extends('layouts.app')

@section('active_modules')
   dropdown active
@endsection

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('content')
  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Ajouter un module de formation pour la CVA  -- {{$cva->intitule}} --</h4>

            <form action="{{'/modules'}}" method="POST" class="form-sample" enctype="multipart/form-data" >
                @csrf
                @method('post')
             
                <div class="form-group" hidden>
                  <input type="text" value="{{$cva->slug}}" id="slug"  class="form-control" name="slug">
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="num_chapitre">Chapitre</label>
                      <input type="text" name="num_chapitre" class="form-control" id="num_chapitre" required>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="intitule">Intitulé du chapitre</label>
                      <input type="text" name="intitule" class="form-control" id="intitule" required>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-6">                    
                    <div class="form-group">
                      <label for="description">Description du contenu du chapitre</label>
                      <textarea name="description" class="form-control" id="description" rows="3" required></textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="language_id">Langue des audios</label>
                      <select class="form-control" name="language_id" id="language_id">
                        @foreach($langues as $langue)
                          <option value="{{$langue->id}}">{{$langue->intitule}}</option>
                        @endforeach
                      </select>                  
                    </div>
                  </div>
                </div>

                <br>
                <div class="form-group">
                  <label class="" for="path_img"> Sélectionnez les images contenues dans ce chapitre</label>
                  <input type="file" id="path_img" name="{{'path_img[]'}}" class="form-control{{ $errors->has('path_img') ? ' is-invalid' : '' }} {{ $errors->has('path_img.*') ? ' is-invalid' : '' }}">

                   @if ($errors->has('path_img'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('path_img') }}</strong>
                                </span>
                    @endif

                    @if ($errors->has('path_img.*'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('path_img.*') }}</strong>
                                </span>
                    @endif     
                </div> 
                <div id="newFieldsPhoto"> </div>

                <button class="btn btn-outline-primary" type="button" onclick="addFieldImage()">Nouvelle image</button>
                <button class="btn btn-outline-danger" type="button" onclick="deleteFieldImage()">Supprimer l'image</button>

                <br><br>  <br>

                <div class="form-group">
                  <label class="" for="path_audio"> Sélectionnez les audios contenus dans ce chapitre</label>
                  <input type="file" id="path_audio" name="{{'path_audio[]'}}" class="form-control{{ $errors->has('path_audio') ? ' is-invalid' : '' }} {{ $errors->has('path_audio.*') ? ' is-invalid' : '' }}"> 

                   @if ($errors->has('path_audio'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('path_audio') }}</strong>
                          </span>
                    @endif
                    @if ($errors->has('path_audio.*'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('path_audio.*') }}</strong>
                          </span>
                    @endif 
                </div>                      
                <div id="newFieldsAudio"> </div>
                      
                <button class="btn btn-outline-primary" type="button" onclick="addFieldAudio()">Nouveau audio</button>

                <button class="btn btn-outline-danger" type="button" onclick="deleteFieldAudio()">Supprimer le fichier audio</button>

                <br><br>  <br> 

                <div class="form-group">
                  <label for="path_video">Sélectionnez des vidéos</label>
                  <input type="file" name="{{'path_video[]'}}" id="path_video" class="form-control{{ $errors->has('path_video') ? ' is-invalid' : '' }} {{ $errors->has('path_video.*') ? ' is-invalid' : '' }}">

                   @if ($errors->has('path_video'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('path_video') }}</strong>
                                </span>
                    @endif 
                    @if ($errors->has('path_video.*'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('path_video.*') }}</strong>
                                </span>
                    @endif 

                </div>

                <div id="newFieldsVideo"> </div>

                <button class="btn btn-outline-primary" type="button" onclick="addFieldVideo()">Nouvelle vidéo</button>
                <button class="btn btn-outline-danger" type="button" onclick="deleteFieldVideo()">Supprimer la vidéo</button>

                <br><br>  <br> 

                <button type="submit" class="btn btn-block btn-success">Enregistrer le module
                </button>

            </form>
          </div>
        </div>                
      </div>
  </div>

@endsection

@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">
        
        var positionImage = 0;
        var positionAudio = 0;
        var positionVideo = 0;

    /*Ajouter champ image*/
        function addFieldImage()
        {
          positionImage+=1;
          var champsImage ="<div class='form-control' id='ligneimg"+positionImage+"'><input type='file' id='path_img' name='path_img[]' class='form-control'> <br><br></div>";

          var ajout = $("#newFieldsPhoto");

          ajout.append(champsImage);
         }

    /*Ajouter champ audio*/
         function addFieldAudio()
        {
           positionAudio+=1;

          var champsAudio ="<div class='form-group' id='ligneaud"+positionAudio+"'><input type='file' id='path_audio' name='path_audio[]' class='form-control'></div>";

          var ajout = $("#newFieldsAudio");

          ajout.append(champsAudio);
         }

         /*Ajouter champ video*/
         function addFieldVideo()
        {
          positionVideo+=1;
          var champsVideo ="<div class='form-group' id='lignevid"+positionVideo+"'><input type='file' id='path_video' name='path_video[]' class='form-control'> </div>";

          var ajout = $("#newFieldsVideo");

          ajout.append(champsVideo);
         }

         /*Supprimer champ image*/
         function deleteFieldImage()
        {
          if (positionImage > 0) {

            let ligne_id = "#ligneimg"+positionImage;
            var ligne_image = $(ligne_id);
            ligne_image.remove();

            positionImage -=1;
          }
         }

         /*Supprimer champ audio*/
         function deleteFieldAudio()
        {
          if (positionAudio > 0) {

            let ligne_id = "#ligneaud"+positionAudio;
            var ligne_audio = $(ligne_id);
            ligne_audio.remove();

            positionAudio -=1;
          }
         }

         /*Supprimer champ vidéo*/
         function deleteFieldVideo()
        {
          if (positionVideo > 0) {

            let ligne_id = "#lignevid"+positionVideo;
            var ligne_video = $(ligne_id);
            ligne_video.remove();

            positionVideo -=1;
          }
         }
    </script>

@endsection