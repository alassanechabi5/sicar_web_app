@extends('layouts.app')

@section('active_modules')
   dropdown active
@endsection

@section('importCss')

<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">
@endsection

@section('breadcrumb')
    MODULES
@endsection

@section('content')

    <a href="{{'/modules-creation/'.$slug_cva}}" class="btn btn-outline-success float-right "><i class="mdi mdi-plus"></i>Ajouter un module
    </a>

    <br> <br> 
    @foreach($list_modules as $one_module)
        <div class="row mb-3">      
          <div class="col-md-12">
            
            <div class="card">

              <div class="card-body">
                <div class="row float-right">
                  <a id="{{'editModule'.$one_module->slug}}" href="{{'/modules/'.$one_module->slug.'/edit'}}" class="btn btn-inverse-success"><i class="mdi mdi-pencil"></i>Editer</a>
                  <button type="button" class="btn btn-inverse-danger" data-target="{{'#supprModule'.$one_module->id}}" data-toggle="modal"><i class="mdi mdi-close"></i>Supprimer</button>

                 {{-- Modal confirmation de suppression d'un module --}}
        <div class="modal fade" id="{{'supprModule'.$one_module->id}}" tabindex="-1" role="dialog" aria-labelledby="moduleSuppr" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Voulez vraiment supprimer?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-footer">
                <form action="{{'/modules/'.$one_module->id}}" method="POST" enctype="multipart/form-data">

                    @method('DELETE')
                    @csrf

                    <button type="submit" class="btn btn-success">Oui</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
              </div>
            </div>
          </div>
        </div>
    {{-- Fin Modal --}}
                </div>
                <h4 class="card-title">{{$one_module->intitule}}</h4>

                <table class="table table-responsive table-bordered table-striped">
                  <thead>                    
                  </thead>

                  <tbody>                    
                    <tr>
                      <td>Chapitre</td>
                      <td>{{$one_module->num_chapitre}}</td>
                    </tr>

                    <tr>
                      <td>Intitulé</td>
                      <td>{{$one_module->intitule}}</td>
                    </tr>

                    <tr>
                      <td>Langue</td>
                      <td>{{$one_module->language->intitule}}</td>
                    </tr>

                    <tr>
                      <td>Description</td>
                      <td>{{$one_module->description}}</td>
                    </tr>

                    @php
                        $images = $one_module->path_img;
                        $tab_images = explode(";", $images);
                        $taille_img = count($tab_images);

                        $audios = $one_module->path_audio;
                        $tab_audios = explode(";", $audios);
                        $taille_audio = count($tab_audios);

                        $videos = $one_module->path_video;
                        $tab_videos = explode(";", $videos);
                        $taille_video = count($tab_videos);
                    @endphp
                    <tr>
                      <td>Images</td>
                      <td>
                        @for($p=0; $p<$taille_img-1; $p++)
                          <img src="{{$tab_images[$p]}}" alt="Img" style="border-radius: 8px;padding: 0 6px; float: left; width: 24.99999%;">
                        @endfor
                      </td>
                    </tr>

                    <tr>
                      <td>Audio</td>
                      <td>
                          @for($q=0; $q<$taille_audio-1; $q++)
                            <audio src="{{$tab_audios[$q]}}" controls></audio>
                            <br>
                          @endfor
                      </td>
                    </tr>

                    <tr>
                      <td>Vidéos</td>
                      <td>
                        @for($r=0; $r<$taille_video-1; $r++)
                            <video src="{{$tab_videos[$r]}}" controls height="150px">{{$tab_videos[$r]}} </video>
                          <br> <br>
                        @endfor
                      </td>
                    </tr>

                  </tbody>
                  
                </table>
              </div>
            </div>

          </div>
        </div>
    @endforeach
    <br> <br> <br>

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>


    <script type="text/javascript">
        
       
    </script>

@endsection