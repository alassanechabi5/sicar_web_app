@extends('layouts.app')

@section('importCss')

  <link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/buttons.bootstrap4.min.css')}}">

@endsection

@section('active_difficultes')
     dropdown active
@endsection

@section('breadcrumb')
   REPONSES AUX REQUETES
@endsection

@section('content')

    <div class="row">

       <div class="col-md-12">
          <div class="card">
          <div class="card-body">
            <blockquote class="blockquote blockquote-primary">
                <ul class="list-group list-group-flush">
                    @foreach($questions as $question)  
                        <div class="list-group-item">
                          <div class="row">
                          <div class="col-md-4"> Producteur: </div>{{$question->relais->full_name}} 
                          </div>
                        </div>

                        <div class="list-group-item">
                          <div class="row">
                          <div class="col-md-4"> CVA: </div> {{$question->cva->intitule}}
                          </div>
                        </div>

                        <div class="list-group-item">
                          <div class="row">
                           <div class="col-md-4"> Requête initiale: </div>

                           <audio controls>
                            <source src="{{url($question->path_audio)}}" type="" />
                           </audio> 

{{--                             <audio class="" controls src="{{$question->path_audio}}"></audio> 
 --}}                          </div>
                        </div>
                    @endforeach    
                </ul> 
              </blockquote>                
          </div>
          </div>
      </div>

       <div class="col-md-12">
         <div class="card">
           <div class="card-body">
              <h4 class="card-title">Liste des réponses à la requête</h4>

              

              <table id="tableReponses" class="table table-responsive table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>Admin</th>
                      {{-- <th>Producteurs</th> --}}
                     {{--  <th>CVA concernée</th> --}}
                      {{-- <th>Requête du producteur</th> --}}
                      <th>Réponses</th>                                
                      <th>Date d'envoi</th>                                
                    </tr>
                  </thead>

                  <tbody>
                      @foreach($reponses as $solution)
                      <tr>
                          <td>{{$solution->user->full_name}}</td>
                          <td>  
                              <audio controls src="{{ url($solution->path_audio) }}"></audio>
                          </td> 

                          <td>{{convertirDate($solution->created_at)}}</td>                      
                      </tr>
                      @endforeach

                  </tbody>
              
              </table>
          </div>
          </div>        
      </div>
    </div>
    
        <br><br><br>

   

@endsection


@section('importJs')

    <script src="{{asset('/js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/jszip.min.js')}}"></script>
    <script src="{{asset('/js/pdfmake.min.js')}}"></script> 
   <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>

    

    <script type="text/javascript">
        
        $(document).ready(function() {
            var table_reponses = $('#tableReponses').DataTable( {
                // dom: 'Blfrtip',
                // responsive: true,
                lengthChange: false,
                // "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],

            });
         
            table_reponses.buttons().container()
                .appendTo('#tableReponses_wrapper .col-md-6:eq(0)');
            
        } );
        
  </script>  

@endsection
