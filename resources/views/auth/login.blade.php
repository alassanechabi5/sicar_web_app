<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SICAR</title>
    
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/ionicons/dist/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.addons.css')}}">

    <link rel="stylesheet" href="{{asset('css/shared/style.css')}}">    
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
  </head>
  <body>

    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">

        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">

          <div class="row w-100">

            <div class="col-lg-4 mx-auto">
               
              <div class="auto-form-wrapper">
                <center><img src="/images/log.jpg" width="50px" height="50px" style="border-radius: 10px;"> <br> <br></center>                

                <h2><div style="margin-top: -30px;margin-left:100px"><span>Connexion</span></div></h2> 

                <form action="{{ route('login') }}" method="POST" >
                  @csrf
                  @method('post')

                  <div class="form-group">

                    <label for="email" class="label">E-mail</label>

                    <div class="input-group">
                      <input name="email" id="email" type="email" required class="form-control" placeholder="Entrez votre adresse email">

                      <div class="input-group-append">
                        <span class="input-group-text">
                          {{-- <i class="mdi mdi-check-circle-outline"></i> --}}
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">

                    <label for="password" class="label">Mot de passe</label>

                    <div class="input-group">
                      <input name="password" id="password" type="password" required class="form-control" placeholder="Entrez votre mot de passe">
                                            
                      <div class="input-group-append">
                        <span class="input-group-text">
                          {{-- <i class="mdi mdi-check-circle-outline"></i> --}}
                        </span>
                      </div>
                    </div>
                  </div>

                    <button type="submit" class="btn btn-primary btn-block">Se connecter</button>
                 
                  <br> <br>

                  <div class="form-group d-flex justify-content-center">
                   

                    <a href="#" class="text-small forgot-password text-black">Mot de passe oublié</a>
                  </div>
 
                </form>


              </div>
              <ul class="auth-footer">
                <li>
                  <a href="#">Conditions</a>
                </li>
                <li>
                  <a href="#">Aide</a>
                </li>
                <li>
                  <a href="#">Termes</a>
                </li>
              </ul>
              <p class="footer-text text-center">Copyright © 2021</p>
              
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('/vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{asset('/js/shared/off-canvas.js')}}"></script>
    <script src="{{asset('/js/shared/misc.js')}}"></script>
    <!-- endinject -->
  </body>
</html>